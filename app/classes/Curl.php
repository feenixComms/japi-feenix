<?php
namespace Feenix\Classes;
use Feenix\Exceptions\HTTPException;

class Curl {
    
	function doRequest($method, $url, $vars) {
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.26) Gecko/20120128 Firefox/3.6.26');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10 );
		curl_setopt($ch, CURLOPT_TIMEOUT, 10 );
		
		if ($method == 'POST') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		}
		
		$content = curl_exec( $ch );
		$response = curl_getinfo( $ch );
		curl_close ( $ch );

		if ($content) {
			return array('SUCCESS',$content,$response);
		} else {
			return array('ERROR',curl_error($ch));

		}
			
	}
	
	function get($url) {
		return $this->doRequest('GET', $url, 'NULL');
	}

	function post($url, $vars) {
		return $this->doRequest('POST', $url, $vars);
	}

	function parse_infinite_values($curl_content){
		$content = explode("\r\n\r\n", $curl_content);
		
		if( count($content) == 2 ){
		
			$infinet_values_rows = explode("\r\n", $content[1]);
			
			$infinet_rows=0;
			
			foreach ( $infinet_values_rows as $infinet_values_row){
				
				$infinet_values = explode("|",$infinet_values_row);
				$x=0;
				foreach ($infinet_values as $infinet_value ){
				
					if( $infinet_rows == 0 ){
						$infinet_data['response_code']= $infinet_value;
						$infinet_rows=1;
						continue;
					}
				
				$infinet_data[$infinet_rows][$x++]= $infinet_value;
				
				}
				$infinet_rows++;
			
			}
			return $infinet_data;
			
		
		} else {

					throw new HTTPException(
											"Unexpected Error occurred",
											401,
											array(
													'dev' => '',
													'internalCode' => ''
													//'more' => '',
													//applicationCode => ''
												)
										);		
		}
	
	}
	function guid(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12)
                .chr(125);// "}"
        return $uuid;
    	}//if the function com_create_guide does not exist
	}//function guid

}// class end

/*
function to handle redirects

function get_url( $url,  $javascript_loop = 0, $timeout = 5 )
{
    $url = str_replace( "&amp;", "&", urldecode(trim($url)) );

    $cookie = tempnam ("/tmp", "CURLCOOKIE");
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_ENCODING, "" );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
    $content = curl_exec( $ch );
    $response = curl_getinfo( $ch );
    curl_close ( $ch );

    if ($response['http_code'] == 301 || $response['http_code'] == 302)
    {
        ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");

        if ( $headers = get_headers($response['url']) )
        {
            foreach( $headers as $value )
            {
                if ( substr( strtolower($value), 0, 9 ) == "location:" )
                    return get_url( trim( substr( $value, 9, strlen($value) ) ) );
            }
        }
    }

    if (    ( preg_match("/>[[:space:]]+window\.location\.replace\('(.*)'\)/i", $content, $value) || preg_match("/>[[:space:]]+window\.location\=\"(.*)\"/i", $content, $value) ) &&
            $javascript_loop < 5
    )
    {
        return get_url( $value[1], $javascript_loop+1 );
    }
    else
    {
        return array( $content, $response );
    }
}
*/
