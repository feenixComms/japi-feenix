<?php
namespace Feenix\Classes;

class MySQLDB{

        function sql_connect($sqlserver, $sqluser, $sqlpassword, $database){
            $this->connect_id = mysqli_connect($sqlserver, $sqluser, $sqlpassword);
            if($this->connect_id){
                if (mysqli_select_db($this->connect_id, $database)){
                    return $this->connect_id;
                }else{
                    return $this->error();
                }
            }else{
                return $this->error();
            }
        }

        function query($query){
            if ($query != NULL){
                $this->query_result = mysqli_query($this->connect_id , $query);
                if(!$this->query_result){
                    return $this->error();
                }else{
                    return $this->query_result;
                }
            }else{
                return '<b>DB Error</b>: Empty Query!';
            }
        }

        function get_num_rows($query_id = ""){
            if($query_id == NULL){
                $return = mysqli_num_rows($this->query_result);
            }else{
                $return = mysqli_num_rows($query_id);
            }
            if(!$return){
                $this->error();
            }else{
                return $return;
            }
        }

        function fetch_row($query_id = ""){
            if($query_id == NULL){
                $return = mysqli_fetch_array($this->query_result);
            }else{
                $return = mysqli_fetch_array($query_id);
            }
            if(!$return){
                $this->error();
            }else{
                return $return;
            }
        }
        function get_affected_rows($query_id = ""){
            if($query_id == NULL){
                $return = mysqli_affected_rows($this->query_result);
            }else{
                $return = mysqli_affected_rows($query_id);
            }
            if(!$return){
                $this->error();
            }else{
                return $return;
            }
        }

        function sql_id(){

           return mysqli_insert_id($this->connect_id);

         }

        function sql_close(){
            if($this->connect_id){
                return mysqli_close($this->connect_id);
            }
        }


        function error(){
            if(mysqli_error($this->connect_id) != ''){
                echo '<b>DB Error</b>: '.mysqli_error($this->connect_id).'<br/>';
            }
        }

}


?>
