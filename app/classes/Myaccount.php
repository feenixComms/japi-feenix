<?php
namespace Feenix\Classes;
use Feenix\Exceptions\HTTPException;

class Myaccount extends \Feenix\Controllers\BaseController{
	
	protected $connection_api_myaccount;
	
	public function __construct(){
	
		parent::__construct();
		
		$this->connection_api_myaccount = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
								"host" => $this->di->getShared('config')->myaccount_api->host,
								"username" => $this->di->getShared('config')->myaccount_api->username,
								"password" => $this->di->getShared('config')->myaccount_api->password,
								"dbname" => $this->di->getShared('config')->myaccount_api->dbname
					));
		return;
	}

	
	public function verifyMyKey() {

		$request = $this->di->get('request');
	
		$id 	= $request->get('id', 'string');
		$key 	= $request->get('key', 'string');

			$sql = "SELECT
						`api_id`
					FROM
						`customer_api_keys`
					WHERE
						`api_id` = '" . $id . "'
					AND 
						`api_key` = '" . $key . "'";
		
		$sql_result = $this->connection_api_myaccount->query($sql);
		
		if($sql_result->numRows()) {
			return TRUE;
		}

		return FALSE;			
				
	}

	public function createMyAccount() {
		
		date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');

		$account 		= $validationObj->validateMyUsername($request->get('username', 'string'));
		$password 		= $validationObj->validateMyPassword($request->get('password', 'string'));
		$template_id 	= $request->get('template_id', 'string');
		$country_code 	= $request->get('country_code', 'string');
		$sip_account 	= $request->get('sip_account', 'string');
		$sip_password 	= $request->get('sip_password', 'string');
		$subject		= "New Account Setup";		

			$sql_country_result		= $this->connection_api_myaccount->query("select * from countries where dialcode = '$country_code' ");
				
				$sql_country_result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
				
				while ($mycountry = $sql_country_result->fetchArray()) {
					$country_id = $mycountry['country_id'];
				}

			$sql_get_template		= "SELECT * FROM templates WHERE template_id = '$template_id'";
			$sql_get_template 		= $this->connection_api_myaccount->query($sql_get_template);

			if( $sql_get_template->numRows() > 0 ){
				$sql_get_template->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
				
				while ($mysettings		= $sql_get_template->fetchArray()) {
						$ratecard_id	 		= $mysettings['ratecard_id'];
						$did_ratecard_id	 	= $mysettings['did_ratecard_id'];
						$dial_pattern_id	 	= $mysettings['dial_pattern_id'];
						$country_id	 			= $mysettings['country_id'];
						$timezone_id	 		= $mysettings['timezone_id'];
						$currency_id	 		= $mysettings['currency_id'];
						$tenant_id	 			= $mysettings['tenant_id'];
						$balance	 			= $mysettings['balance'];
						$channels	 			= $mysettings['channels'];
						$low_balance_val	 	= $mysettings['low_balance_val'];
						$account_type			= 'PARENT';	
						$vm_tz					= 'NZT';
				}
			}
			//else set the default values of the account
			else {
						$ratecard_id	 		= 10;
						$did_ratecard_id	 	= 1;
						$dial_pattern_id	 	= 6;
						$country_id	 			= ( $country_id=='' ? '226' : $country_id );
						$timezone_id	 		= 1;
						$currency_id	 		= 2;
						$tenant_id	 			= 2;
						$balance	 			= 0;
						$channels	 			= 2;
						$low_balance_val	 	= '0';
						$account_type			= 'PARENT';	
						$vm_tz					= 'NZT';			
			}
			
			$codecs 				= 'alaw;ulaw;gsm;g729';
			$creation_date 			= date('Y/m/d H:i:s');
			$sip_pass				= rand(1000,9999);
			/*
			//now create an account
			$sql_make_account 	= $this->connection_api_myaccount->query("INSERT INTO users(name,email,parent_account,password,account_type,current_balance,created_at) VALUES('$account','$email','$account',md5('$password'),'$account_type','$balance',UNIX_TIMESTAMP()) ");
			$user_id			= $this->connection_api_myaccount->lastInsertId();
			
			if(!empty($user_id)) {
				$update_users_account = $this->connection_api_myaccount->query("UPDATE 	users_accounts SET is_free='N' , assignment_date = '$creation_date' WHERE account_name='$account'");	
				
				//make the default pool
				$sql_make_pool	= $this->connection_api_myaccount->query("INSERT INTO did_pools(user_id,pool_name,pool_channels,pool_desc) values('$user_id','Default','2','Default')");
				$pool_id		= $this->connection_api_myaccount->lastInsertId();
				
			}
			$sql_make_settings 	= $this->connection_api_myaccount->query("INSERT INTO users_settings(user_id,client_code,ratecard_id,tenant_id,country_id,currency_id,dial_pattern_id,current_statement_date,next_statement_date,timzone_id,low_balance_val) values('$user_id','$account','$ratecard_id','$tenant_id','$country_id','$currency_id','$dial_pattern_id','$current_statement_date','$next_statement_date','$timezone_id','$low_balance_val')");
			$make_users_contact_detail 	= $this->connection_api_myaccount->query("INSERT INTO users_contact_detail(user_id,first_name,last_name,primary_email,phone_no) VALUES('$user_id','$first_name','$last_name','$email','$phone')");
			$make_sip_user 		= $this->connection_api_myaccount->query("INSERT INTO sip_users(user_id,username,name,defaultuser,secret,allow) values('$user_id','$sip_account','$sip_account','$sip_account','$sip_password','$codecs')");
			$make_vm_user 		= $this->connection_api_myaccount->query("INSERT INTO voicemails(customer_id,mailbox,password,fullname,email,tz) values('$sip_account','$sip_account','$sip_account','$first_name','$email','$vm_tz')");
			$make_conf_user 	= $this->connection_api_myaccount->query("INSERT INTO meetme(confno,pin,adminpin,maxusers,email,user_id) values('$sip_account','$user_pin','$admin_pin','10','$email.com','$user_id')");
			$make_default_gw 	= $this->connection_api_myaccount->query("INSERT INTO users_dispatcher_detail(dispatcher_name,user_id) values('Default','$user_id')");
			$make_rt_access		= $this->connection_api_myaccount->query("INSERT INTO users_ratecards(user_id,ratecard_id,did_ratecard_id) VALUES('$user_id','$ratecard_id','$did_ratecard_id')");
			
			$email_data	= "Congratulations on choosing Feenix! We know that you will be impressed with our unique service!<br />

							
							If you are having troubles, please contact us at: support@feenix.co.nz.<br />";
					$this->connection_api_myaccount->query("insert into cron_emails(email_type,user_id,name,full_name,subject,to_email,bcc_email,email_data,insert_time) VALUES('$email_type','$user_id','$user','$full_name','$subject','$email','$support_email','$email_data','$today')");
					*/
					
					$results = array(
							'account' => $account,
							'sipID' => $sip_account,
							'sipPass' => $sip_pass,
							'vmID' => $sip_account,
							'next_statement_date' => $next_statement_date						
							);		
			
			return $results;
							
	}		

public function createChildAccount() {

		date_default_timezone_set('UTC');
		
		$request 		= $this->di->get('request');
		
		$username = $request->get('username', 'string');
		$password = $request->get('password', 'string');
						
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$username 		= $validationObj->validateMyUsername($username);
		$password		= $validationObj->validateMyPassword($password);
		$password		= md5($password);
		
		$first_name 	= $request->get('first_name', 'string');
		$last_name 		= $request->get('last_name', 'string');
		$full_name		= ( $request->get('first_name', 'string')=='' ? 'Customer' : "$first_name $last_name" );;
		$email 			= $validationObj->validateMyEmail($request->get('email', 'string'));
		$phone 			= $request->get('phone', 'string');
		$timezone_id 	= ( $request->get('timezone_id', 'string')=='' ? '10' : $request->get('timezone_id', 'string') ); //default Australia/Sydney
		$send_acct_email= $validationObj->validatebolean($request->get('send_acct_email', 'string'));
		$act_range		= "'834','835','836','837','838'"; //free account range
		$no_of_act		= 1; //default create only 1 account
		$codecs 				= 'g729;ulaw;alaw;gsm;ilbc;speex';
		$today					= date('Y/m/d');
		$current_statement_date = date('Y/m/d');
		$creation_date 			= date('Y/m/d H:i:s');
		//$next_statement_date	= date('Y/m/d', strtotime('+1 months'));
		$next_statement_date	= date("Y-m", strtotime(date('Y-m')." +1 month"))."-02";
		$admin_pin				= rand(1000,9999);
		$user_pin				= rand(1000,9999);
		$email_type				= "Child Account Setup";
		$subject				= "FeeniX: Account Setup";
		
			$sql_get_parent		= "SELECT
						users.name AS username,
						users.customer_type,
						users.`password`,
						users.`customer_type`,
						users.account_type,
						users.parent_account,
						users.account_mode,
						users.account_mode_time,
						users.email,
						users.current_balance,
						users.id,
						users_settings.client_code,
						users_settings.ratecard_id,
						users_settings.billing_type,
						users_settings.auto_deduction,
						users_settings.auto_topup_type,
						users_settings.auto_topup_source,
						users_settings.auto_topup_amount,
						users_settings.tenant_id,
						users_settings.channels,
						users_settings.language,
						users_settings.country_id,
						users_settings.currency_id,
						users_settings.dial_pattern_id,
						users_settings.account_service_type,
						users_settings.cc_charge_max_amount,
						users_settings.cc_charge_invalid_tries,
						users_settings.current_statement_date,
						users_settings.next_statement_date,
						users_settings.billing_accuracy,
						users_settings.credit_line,
						users_settings.account_service_type,
						users_settings.email_template,
						users_settings.email_from_address,
						tenants.tenant_ip,
						tenants.tenant_status,
						countries.name AS country_name,
						countries.dialcode,
						countries.iso,
						countries.nicename AS country_name_display,
						currencies.currency,
						currencies.currency_display,
						timezones.timezone_id,
						timezones.timezone,
						users_contact_detail.first_name,
						users_contact_detail.primary_email,
						users_contact_detail.secondary_email,
						users_contact_detail.noc_email,
						users_contact_detail.support_email,
						sip_users.outbound_callerid,
						sip_users.route_timeout
						FROM
						users
						Inner Join users_contact_detail ON users.id = users_contact_detail.user_id
						Inner Join users_settings ON users.id = users_settings.user_id
						Inner Join tenants ON users_settings.tenant_id = tenants.tenant_id
						Inner Join currencies ON users_settings.currency_id = currencies.currency_id
						Inner Join countries ON users_settings.country_id = countries.country_id
						Inner Join timezones ON users_settings.timzone_id = timezones.timezone_id
						Inner Join sip_users ON users_settings.user_id = sip_users.user_id
						WHERE
						users.`name` = '" . $username . "' AND (users.`password` = '" . $password . "' OR users.`password`='".$request->get('password', 'string')."')  AND users.active='Y' AND users.account_type='PARENT' AND users.account_mode!='Cancelled'";
			$sql_get_parent 		= $this->connection_api_myaccount->query($sql_get_parent);

			if( $sql_get_parent->numRows() > 0 ){
				$sql_get_parent->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
				
				while ($mysettings		= $sql_get_parent->fetchArray()) {
						$ratecard_id	 		= $mysettings['ratecard_id'];
						$dial_pattern_id	 	= $mysettings['dial_pattern_id'];
						$country_id	 			= $mysettings['country_id'];
						$currency_id	 		= $mysettings['currency_id'];
						$tenant_id	 			= $mysettings['tenant_id'];
						$balance	 			= $mysettings['current_balance'];
						$channels	 			= $mysettings['channels'];
						$billing_accuracy	 	= $mysettings['billing_accuracy'];
						$account_service_type	= $mysettings['account_service_type'];
						$credit_line			= $mysettings['credit_line'];
						$outbound_callerid		= $mysettings['outbound_callerid'];
						$route_timeout			= $mysettings['route_timeout'];
						$email_from_address		= $mysettings['email_from_address'];
						$email_template			= $mysettings['email_template'];
						$low_balance_val	 	= 0;
						$cc_charge_max_amount	= 500;
						$cc_charge_invalid_tries= 3;
						$account_type			= 'CHILD';	
						$vm_tz					= 'PER';
				}
			}
			//else cant create the child account
			else {
						throw new HTTPException(
												'Unable to login via parent account',
												402,
												array(
														'dev' => '102',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);
														
			}
			
		
			
			$sql_available_account 	= "SELECT * FROM users_accounts WHERE substring(account_name,1,3) in ($act_range) AND is_free = 'Y' ORDER BY account_name LIMIT $no_of_act ";
			$sql_available_account	= $this->connection_api_myaccount->query($sql_available_account);
				
				$sql_available_account->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
				
				while ($free_acct = $sql_available_account->fetchArray()) {
					$acct[] 	  = $free_acct['account_name'];
				}			

			$account 		  = $acct[0];
			$account_password = substr(str_shuffle("abcdefghijkBCDEFGHIJKLM@NOlmnopqrstuvwxyzAPQRSTUVWXYZ0123456789"),0,8);
			$sip_account 	  = $account;
			$sip_password 	  = $account_password;


			if( empty($account) ) {
						throw new HTTPException(
												'Unable to find the account',
												417,
												array(
														'dev' => '117',
														'internalCode' => ""
														//'more' => '',
														//applicationCode => ''
													)
												);				
			}
					
			//now create an account
			$sql_make_account 	= $this->connection_api_myaccount->query("INSERT INTO users(name,email,parent_account,password,account_type,current_balance,created_at) VALUES('$account','$email','$username',md5('$account_password'),'$account_type','$balance',UNIX_TIMESTAMP()) ");
			$user_id			= $this->connection_api_myaccount->lastInsertId();
			
			if(!empty($user_id)) {
				$update_users_account = $this->connection_api_myaccount->query("UPDATE 	users_accounts SET is_free='N' , assignment_date = '$creation_date' WHERE account_name='$account'");	
			}
			
			if($outbound_callerid=='Dynamic') {
				$this->connection_api_myaccount->query("INSERT INTO identifiations(user_id , caller_display) values('$user_id','$outbound_callerid')");	
			}
			
			$sql_make_settings 	= $this->connection_api_myaccount->query("INSERT INTO users_settings(user_id,client_code,ratecard_id,tenant_id,country_id,currency_id,dial_pattern_id,current_statement_date,next_statement_date,timzone_id,low_balance_val,billing_accuracy,account_service_type,credit_line,email_template,email_from_address) values('$user_id','$account','$ratecard_id','$tenant_id','$country_id','$currency_id','$dial_pattern_id','$current_statement_date','$next_statement_date','$timezone_id','$low_balance_val','$billing_accuracy','$account_service_type','$credit_line','$email_template','$email_from_address')");
			$make_users_contact_detail 	= $this->connection_api_myaccount->query("INSERT INTO users_contact_detail(user_id,first_name,last_name,primary_email,phone_no) VALUES('$user_id','$first_name','$last_name','$email','$phone')");
			$make_sip_user 		= $this->connection_api_myaccount->query("INSERT INTO sip_users(user_id,username,name,defaultuser,secret,allow,outbound_callerid,route_timeout) values('$user_id','$sip_account','$sip_account','$sip_account','$sip_password','$codecs','$outbound_callerid','$route_timeout')");
			$make_vm_user 		= $this->connection_api_myaccount->query("INSERT INTO voicemails(customer_id,mailbox,password,fullname,email,tz) values('$sip_account','$sip_account','$sip_account','$first_name','$email','$vm_tz')");
			$make_conf_user 	= $this->connection_api_myaccount->query("INSERT INTO meetme(confno,pin,adminpin,maxusers,email,user_id) values('$sip_account','$user_pin','$admin_pin','10','$email.com','$user_id')");
			
			$email_data			= "Congratulations on choosing FeeniX! We know that you will be impressed with our unique service!<br />

							The details of your account are:<br />
							Account number and Internet Phone number = <strong>$sip_account</strong><br />
							Password = <strong>$sip_password</strong><br />
							<br /><br />
							Please feel free to share FeeniX with your family and friends. The advantage of doing so is you will be able to call each other for free. We also have excellent rates for calling people who do not have MondoTalk. For more information please visit our website www.feenix.co.nz .<br /><br />
							<strong>BYO SIP ATA</strong>
							<br /><br />
							You can use your own SIP complaint ATA, simply set the following in your ATA:<br />
							SIP PORT: 5060<br />
							SIP Server: sip.feenix.co.nz<br />
							SIP Proxy: sip.feenix.co.nz<br />
							SIP Domain: sip.feenix.co.nz<br />
							SIP Username: $sip_account<br />
							SIP Auth: $sip_account<br />
							Password: $sip_password<br />
							Fax: T.38<br />
							Use SIP Proxy: Yes<br />
							Preferred Codec: G.729a, G.711u, G.711a<br />
							SIP Registration Timeout: 180 seconds<br />
							If you want to buy call credit, please visit the below link:<br />
							http://feenix.co.nz/help ?<br /><br />
							If you are having troubles, please contact us at: support@feenix.co.nz In the email please include the SIP ATA make and model.<br />";
					if( $send_acct_email == 'YES' ) {
						$this->connection_api_myaccount->query("insert into cron_emails(email_type,user_id,name,full_name,subject,to_email,bcc_email,email_data,insert_time) VALUES('$email_type','$user_id','$account','$full_name','$subject','$email','$support_email','$email_data','$today')");
					}
					
					$results = array(
							'user_id' => $user_id,
							'username' => $account,
							'login_password' => trim($account_password),
							'login_password_encrypted' => md5($account_password),
							'first_name' => $first_name,
							'email' => $email,
							'sip_name' => $sip_account,
							'sip_password' => $sip_password,
							'vm_id' => $sip_account,
							'conf_id' => $sip_account,
							'next_statement_date' => $next_statement_date						
							);		
			
			return $results;
	
	
}




	public function getChildAccount() {
		date_default_timezone_set('UTC');
		
		$request 		= $this->di->get('request');
		
		$username = $request->get('username', 'string');
		$password = $request->get('password', 'string');
		$search_account  = $request->get('account', 'string');
						
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$username 		= $validationObj->validateMyUsername($username);
		$password		= $validationObj->validateMyPassword($password);
		$password		= md5($password);
		$results		= array();
		$account		= array();

			$sql_get_parent		= "SELECT * FROM users WHERE users.`name` = '" . $username . "' AND (users.`password` = '" . $password . "' OR users.`password`='".$request->get('password', 'string')."')  AND users.active='Y' AND users.account_type='PARENT' AND users.account_mode!='Cancelled'";
			$sql_get_parent 		= $this->connection_api_myaccount->query($sql_get_parent);

			if( $sql_get_parent->numRows() > 0 ){
				$sql_get_parent->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
				
				while ($mysettings		= $sql_get_parent->fetchArray()) {
						$balance	 	= $mysettings['current_balance'];
				}
			}
			//else cant show the child accounts you have to use the parent credentials
			else {
						throw new HTTPException(
												'Unable to login via parent account',
												402,
												array(
														'dev' => '102',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);
														
			}
			

		$sql_account = "SELECT
						users.name AS username,
						users.customer_type,
						users.`password`,
						users.`customer_type`,
						users.account_type,
						users.parent_account,
						users.account_mode,
						users.account_mode_time,
						users.email,
						users.current_balance,
						users.id,
						users_settings.language,
						users_settings.recording,
						users_settings.recording_format,
						users_settings.send_recording_email,
						users_settings.fax,
						users_settings.fax_file_format,
						users_settings.send_fax_email,
						users_settings.fax_send,
						users_settings.ratecard_id,
						users_settings.billing_type,
						users_settings.auto_deduction,
						users_settings.auto_topup_type,
						users_settings.auto_topup_source,
						users_settings.auto_topup_amount,
						users_settings.channels,
						users_settings.max_in_channel,
						users_settings.country_id,
						users_settings.dialplan_blacklists_id,
						users_settings.next_statement_date,
						countries.name AS country_name,
						countries.dialcode,
						countries.iso,
						countries.nicename AS country_name_display,
						timezones.timezone_id,
						timezones.timezone,
						users_contact_detail.first_name,
						users_contact_detail.last_name,
						users_contact_detail.primary_email,
						users_contact_detail.secondary_email,
						users_contact_detail.noc_email,
						users_contact_detail.support_email,
						users_contact_detail.phone_no,
						users_contact_detail.city,
						users_contact_detail.state,
						users_contact_detail.zip,
						users_contact_detail.address,
						users_contact_detail.recording_email,
						users_contact_detail.fax_email,
						sip_users.name as sip_name,
						sip_users.secret,
						sip_users.auth_type,
						dial_pattern_blacklists.dialplan_blacklist_name
						FROM
						users
						Inner Join users_contact_detail ON users.id = users_contact_detail.user_id
						Inner Join users_settings ON users.id = users_settings.user_id
						Inner Join countries ON users_settings.country_id = countries.country_id
						Inner Join timezones ON users_settings.timzone_id = timezones.timezone_id
						Inner Join sip_users ON users.id = sip_users.user_id 
						Left  Join dial_pattern_blacklists ON users_settings.dialplan_blacklists_id = dial_pattern_blacklists.dialplan_blacklist_id 
						WHERE users.parent_account = '$username' AND users.account_type = 'CHILD' AND users.account_mode!='Cancelled' AND users.active = 'Y' and (users_contact_detail.first_name like '$search_account%')";
		
				$sql_account_result	= $this->connection_api_myaccount->query($sql_account);

				
				$sql_account_result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
				
				while ($mylogin = $sql_account_result->fetchArray()) {
					$account[$mylogin['username']]['name'] 		     = $mylogin['username'];
					$account[$mylogin['username']]['login_password'] = $mylogin['password'];
					$account[$mylogin['username']]['first_name'] 	 = $mylogin['first_name'];
					$account[$mylogin['username']]['last_name'] 	 = $mylogin['last_name'];
					$account[$mylogin['username']]['phone_no'] 	 	 = $mylogin['phone_no'];
					$account[$mylogin['username']]['address'] 	 	 = $mylogin['address'];
					$account[$mylogin['username']]['city'] 	 		 = $mylogin['city'];
					$account[$mylogin['username']]['state'] 	 	 = $mylogin['state'];
					$account[$mylogin['username']]['zip'] 	 		 = $mylogin['zip'];
					$account[$mylogin['username']]['email'] 		 = $mylogin['email'];
					$account[$mylogin['username']]['alt_email'] 	 = $mylogin['noc_email'];
					$account[$mylogin['username']]['channels'] 		 = $mylogin['channels'];
					$account[$mylogin['username']]['max_in_channel'] = $mylogin['max_in_channel'];
					$account[$mylogin['username']]['language'] 		= $mylogin['language'];
					$account[$mylogin['username']]['country_id'] 	= $mylogin['country_id'];
					$account[$mylogin['username']]['country_name'] 	= $mylogin['country_name_display'];
					$account[$mylogin['username']]['timezone_id'] 	= $mylogin['timezone_id'];
					$account[$mylogin['username']]['timezone_name'] = $mylogin['timezone'];
					$account[$mylogin['username']]['blacklist_rule_id'] = $mylogin['dialplan_blacklists_id'];
					$account[$mylogin['username']]['blacklist_rule_name'] = $mylogin['dialplan_blacklist_name'];
					$account[$mylogin['username']]['recording'] 	= $mylogin['recording'];
					$account[$mylogin['username']]['recording_format'] = $mylogin['recording_format'];
					$account[$mylogin['username']]['send_recording_email'] = $mylogin['send_recording_email'];
					$account[$mylogin['username']]['recording_email'] = $mylogin['recording_email'];
					$account[$mylogin['username']]['fax'] = $mylogin['fax'];
					$account[$mylogin['username']]['send_fax'] = $mylogin['fax_send'];
					$account[$mylogin['username']]['fax_format'] = $mylogin['fax_file_format'];
					$account[$mylogin['username']]['send_fax_email'] = $mylogin['send_fax_email'];
					$account[$mylogin['username']]['fax_email'] = $mylogin['fax_email'];
					$account[$mylogin['username']]['sip_name']   	= $mylogin['sip_name'];
					$account[$mylogin['username']]['sip_password']  = $mylogin['secret'];
					$account[$mylogin['username']]['sip_auth_type'] = $mylogin['auth_type'];
					$account[$mylogin['username']]['account_mode']  = $mylogin['account_mode'];
				}
	
			if( $sql_account_result->numRows() > 0 ) {
			
				$results = $account;
				
			}
			else{
						throw new HTTPException(
												'Unable to find any child',
												419,
												array(
														'dev' => '102',
														'internalCode' => $sql_account
														//'more' => '',
														//applicationCode => ''
													)
												);
			}
			
			return $results;
		
	}

	public function updateSIPAccount() {
		date_default_timezone_set('UTC');
		
		$request 		= $this->di->get('request');
		
		$account 			= $request->get('account', 'string');
		$sip_line 			= $request->get('sip_line', 'string');
		$sip_display_name 	= $request->get('sip_display_name', 'string');
		$sip_password		= ( $request->get('sip_password', 'string')=='' ? "1234567" : $request->get('sip_password', 'string') );		
		$voicemail_password	= $request->get('voicemail_password', 'string');
		$voicemail_email	= $request->get('voicemail_email', 'string');
		$route_type			= $request->get('route_type', 'string');
		$route_value		= ( $request->get('route_value', 'string')=='' ? $sip_line : $request->get('route_value', 'string') );
		$ring_timeout		= ( $request->get('ring_timeout', 'string')=='' ? "60" : $request->get('ring_timeout', 'string') );
		$route_phone_busy				= $request->get('route_phone_busy', 'string');
		$route_phone_noanswer			= $request->get('route_phone_noanswer', 'string');
		$route_phone_offline			= $request->get('route_phone_offline', 'string');
		$route_unconditional_type			= $request->get('route_unconditional_type', 'string');
		$route_unconditional_value			= $request->get('route_unconditional_value', 'string');
		//$channels 						= $request->get('channels', 'string');
								
		$validationObj 	= new \Feenix\Classes\Validation();

		$results			= array();
				
			
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$voice_db_user    = $this->di->getShared('config')->voice_test->username;
					$voice_db_pass    = $this->di->getShared('config')->voice_test->password;
					$voice_db_host    = $this->di->getShared('config')->voice_test->host;
					$voice_db     	= $this->di->getShared('config')->voice_test->dbname;
				}
				else {
					$voice_db_user    = $this->di->getShared('config')->voice->username;
					$voice_db_pass    = $this->di->getShared('config')->voice->password;
					$voice_db_host    = $this->di->getShared('config')->voice->host;
					$voice_db     	= $this->di->getShared('config')->voice->dbname;
				}
                $NewDBOBJ->sql_connect($voice_db_host,$voice_db_user,$voice_db_pass, $voice_db);
			
			$sql_account 	 = "SELECT users.id,users.name,(select sipusers.id from sipusers where sipusers.name='$sip_line') as sipusers_id from users WHERE users.account='$account' ";
				
			$sql_account_result 		= $NewDBOBJ->query($sql_account);
		
			if( $NewDBOBJ->get_num_rows() > 0 ){

					while ($myaccount = $NewDBOBJ->fetch_row($sql_account_result)) {
							$user_id  	= $myaccount['id'];
							$sipusers_id= $myaccount['sipusers_id'];
							$accound_id	= $user_id;
					}

					if( empty($sipusers_id) ) {
								throw new HTTPException(
														'Unable to locate the sip account',
														418,
														array(
																'dev' => '118',
																'internalCode' => ''
																//'more' => '',
																//applicationCode => ''
															)
														);
					}

					
					//if sip password is 1234567 or empty then don't update 
					if( $sip_password!='1234567' )  {
						$NewDBOBJ->query("UPDATE sipusers set `secret`='$sip_password' WHERE user_id='$user_id' and name='$sip_line'");			
					}
					
					
					$update_sip_user = $NewDBOBJ->query("UPDATE sipusers set `outbound_callerid_name`='$sip_display_name',route_type='$route_type',route_val='$route_value',route_timeout='$ring_timeout',route_phone_busy='$route_phone_busy',route_phone_noanswer='$route_phone_noanswer',route_phone_offline='$route_phone_offline',route_always_forward_type='$route_unconditional_type',route_always_forward_forward='$route_unconditional_value' WHERE user_id='$user_id' and name='$sip_line'");			
					$update_voicemail_user = $NewDBOBJ->query("UPDATE voicemails set fullname='$sip_display_name', email = '$voicemail_email',password='$voicemail_password' where mailbox='$sip_line' ");
					
					if( $update_sip_user ) {
					
							$results[] = array(
									'accound_id' => $accound_id,
									'route_id' => $sipusers_id,
									'sip_line' => $sip_line,
									'msg' => "Your SIP account has been updated"
									);		
														
						
					}
					else{
								throw new HTTPException(
														'Unable to update the sip account',
														419,
														array(
																'dev' => '119',
																'internalCode' => ''
																//'more' => '',
																//applicationCode => ''
															)
														);
					}
									
				
			}//if valid account
				
			else{
						throw new HTTPException(
												'Unable to authenticate the account',
												402,
												array(
														'dev' => '102',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);
			}

			return $results;
		
	}	



	public function updatePhoneAccount() {
		date_default_timezone_set('UTC');
		
		$request 		= $this->di->get('request');
		
		$account 			= $request->get('account', 'string');
		$did 				= $request->get('phone', 'string');
		$did_notes 			= $request->get('notes', 'string');
		$did_status			= ( $request->get('status', 'string')=='' ? "UNALLOCATED" : $request->get('status', 'string'));				
		$did_route_id		= $request->get('phone_route_id', 'string');
		$route_type				= $request->get('route_type', 'string');
		
		//$channels 							= $request->get('channels', 'string');
								
		$validationObj 	= new \Feenix\Classes\Validation();

		
		$today				= date("Y-m-d H:i:s");
		$results			= array();
				
			
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$voice_db_user    = $this->di->getShared('config')->voice_test->username;
					$voice_db_pass    = $this->di->getShared('config')->voice_test->password;
					$voice_db_host    = $this->di->getShared('config')->voice_test->host;
					$voice_db     	= $this->di->getShared('config')->voice_test->dbname;
				}
				else {
					$voice_db_user    = $this->di->getShared('config')->voice->username;
					$voice_db_pass    = $this->di->getShared('config')->voice->password;
					$voice_db_host    = $this->di->getShared('config')->voice->host;
					$voice_db     	= $this->di->getShared('config')->voice->dbname;
				}
                $NewDBOBJ->sql_connect($voice_db_host,$voice_db_user,$voice_db_pass, $voice_db);
			
				if( empty($did_route_id) ) {
								throw new HTTPException(
														'Unable to locate the route',
														431,
														array(
																'dev' => '131',
																'internalCode' => ''
																//'more' => '',
																//applicationCode => ''
															)
														);				
				}


			$sql_sipusers_account 	 = "SELECT * FROM sipusers WHERE id = '$did_route_id'";
				
			$sql_sipusers_account_result 		= $NewDBOBJ->query($sql_sipusers_account);
		
			if( $NewDBOBJ->get_num_rows() < 1 ){
								throw new HTTPException(
														'Unable to locate the route',
														431,
														array(
																'dev' => '131',
																'internalCode' => ''
																//'more' => '',
																//applicationCode => ''
															)
														);				
			}
			
			
			$sql_account 	 = "SELECT users.id,users.name,users.account,sipusers.id as sipusers_id,sipusers.name as sipusers_name from users Inner Join sipusers ON users.id = sipusers.user_id WHERE users.account='$account' and sipusers.id='$did_route_id' ";
				
			$sql_account_result 		= $NewDBOBJ->query($sql_account);
		
			if( $NewDBOBJ->get_num_rows() > 0 ){

					while ($myaccount = $NewDBOBJ->fetch_row($sql_account_result)) {
							$user_id  	= $myaccount['id'];
							$sipusers_id= $myaccount['sipusers_id'];
							$sip_line	= $myaccount['sipusers_name'];
							$accound_id	= $user_id;
					}

					if( empty($sipusers_id) ) {
								throw new HTTPException(
														'Unable to locate the sip account',
														418,
														array(
																'dev' => '118',
																'internalCode' => ''
																//'more' => '',
																//applicationCode => ''
															)
														);
					}

					
					//now check the phone number if its valid
					$sql_phone_result 		= $NewDBOBJ->query("SELECT dids.id, users_dids.user_id FROM dids Inner Join users_dids ON dids.id = users_dids.did_id WHERE dids.did='$did' ");
					while ($myphone = $NewDBOBJ->fetch_row($sql_phone_result)) {
							$did_id		= $myphone['id'];
					}
					
					if( empty($did_id) ) {
								throw new HTTPException(
														'Unable to locate the phone',
														432,
														array(
																'dev' => '132',
																'internalCode' => ''
																//'more' => '',
																//applicationCode => ''
															)
														);						
					}
					
					$NewDBOBJ->query("UPDATE users_dids SET did_notes='$did_notes',modify_date='$today' WHERE did_id='$did_id' ");
					$update_phone_user = $NewDBOBJ->query("UPDATE did_routes SET route_id='$did_route_id' WHERE did='$did' ");			

					if( $update_phone_user ) {
					
							$results[] = array(
									'accound_id' => $accound_id,
									'phone_route_id' => $did_route_id,
									'msg' => "Your Phone has been updated"
									);		
														
						
					}
					else{

								throw new HTTPException(
														'Unable to update the phone',
														433,
														array(
																'dev' => '133',
																'internalCode' => ''
																//'more' => '',
																//applicationCode => ''
															)
														);
					}
									
				
			}//if valid account
				
			else{
						throw new HTTPException(
												'Unable to authenticate the account',
												402,
												array(
														'dev' => '102',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);
			}

			return $results;
		
	}	
	public function addPhoneAccount() {
		date_default_timezone_set('UTC');
		
		$request 		= $this->di->get('request');
		
		$did			= $request->get('phone', 'string');	
		$did_city		= $request->get('phone_location', 'string');	
		$did_prefix		= $request->get('phone_prefix', 'string');
		$did_notes		= $request->get('phone_notes', 'string');
		$account		= $request->get('account', 'string');	
		$sip_line		= $request->get('sip_line', 'string');	
		$country_code	= ( $request->get('country_code', 'string')=='' ? "64" : $request->get('country_code', 'string'));
		$sip_display_name	= $request->get('sip_display_name', 'string');	
		$sip_password			= $request->get('sip_password', 'string');
		$voicemail_email		= $request->get('voicemail_email', 'string');	
		$outbound_callerid 		= preg_replace('/^0?/', '', $did);//replace leading 0 (spark accepts like that)
		$outbound_callerid_name	= $sip_display_name;		
		$did_status				= 'ALLOCATED';	
				
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$today				= date("Y-m-d H:i:s");
		$results			= array();
		$sip				= array();
		$phone				= array();
				
			
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$voice_db_user    = $this->di->getShared('config')->voice_test->username;
					$voice_db_pass    = $this->di->getShared('config')->voice_test->password;
					$voice_db_host    = $this->di->getShared('config')->voice_test->host;
					$voice_db     	= $this->di->getShared('config')->voice_test->dbname;
				}
				else {
					$voice_db_user    = $this->di->getShared('config')->voice->username;
					$voice_db_pass    = $this->di->getShared('config')->voice->password;
					$voice_db_host    = $this->di->getShared('config')->voice->host;
					$voice_db     	= $this->di->getShared('config')->voice->dbname;
				}
                $NewDBOBJ->sql_connect($voice_db_host,$voice_db_user,$voice_db_pass, $voice_db);


			$sql_account_did_result 		= $NewDBOBJ->query("SELECT * FROM dids WHERE did='$did'");
		
			if( $NewDBOBJ->get_num_rows() > 0 ){
						throw new HTTPException(
												'Unable to add the phone number',
												439,
												array(
														'dev' => '139',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);				
			}

			$sql_account_sip_result 		= $NewDBOBJ->query("SELECT * FROM sipusers WHERE username='$sip_line'");
		
			if( $NewDBOBJ->get_num_rows() > 0 ){
						throw new HTTPException(
												'Unable to add the sip account',
												438,
												array(
														'dev' => '138',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);				
			}			
				
			$sql_account_result 		= $NewDBOBJ->query("SELECT * FROM users WHERE account='$account'");
		
			if( $NewDBOBJ->get_num_rows() > 0 ){

				while ($myacct = $NewDBOBJ->fetch_row($sql_account_result)) {
					$account_id		= $myacct['id'];
					$account_name	= $myacct['name'];
					$customer_type	= $myacct['customer_type'];
					$account_mode	= $myacct['account_mode'];
				}
				
				//add the phone number now 
				$NewDBOBJ->query("insert into dids(did,city,did_prefix,provsioning_status) values('$did','$did_city','$did_prefix','$did_status')");
				
				$did_id = $NewDBOBJ->sql_id();
				
				//assign the DID to the customer
				$NewDBOBJ->query("insert into users_dids(user_id,did_id,did_notes,assignment_date,modify_date) values('$account_id','$did_id','$did_notes','$today','$today')");
				
				//create the actual sip user (sip user with password)
				if( empty($sip_line) ) {
						$sip_line 		= preg_replace('/^0?/', $country_code, $did);	
						$sip_password 	= substr(str_shuffle('mnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123abcdefghijkl456789') , 0 , 10 );
						
				}
				$NewDBOBJ->query("insert into sipusers(username,name,defaultuser,outbound_callerid,outbound_callerid_name,secret,user_id,account) values('$sip_line','$sip_line','$sip_line','$outbound_callerid','$outbound_callerid_name','$sip_password','$account_id','$account')");
				$sip_id = $NewDBOBJ->sql_id();
				
				//create did route last step ;) route_id is the sipusers.id i.e $sip_id
				
				$did_route_id = $NewDBOBJ->query("insert into did_routes(did,route_id) values('$did','$sip_id')");
				
				//create a voicemail box
				$voicemail	 		= $sip_line;
				$voicemail_password = rand(1000,9999);
				
				$NewDBOBJ->query("insert into voicemails(customer_id,mailbox,password,fullname,email) values('$sip_line','$sip_line','$voicemail_password','$sip_display_name','$voicemail_email')");

					$phone[$did]['phone'] 		= $did;
					$phone[$did]['phone_prefix'] 	= $did_prefix;
					$phone[$did]['phone_location'] 	= $did_city;
					$phone[$did]['phone_route_id'] 	= $did_route_id;
					$phone[$did]['phone_route_type'] 	= 'INTERNAL_SIP';
					$phone[$did]['phone_route_to_sip'] 	= $sip_line;
					$phone[$did]['notes'] 				= $did_notes;
					$phone[$did]['status'] 				= $did_status;

					$sip[$sip_line]['sip_line'] 					= $sip_line;
					$sip[$sip_line]['sip_auth_username'] 			= $sip_line;
					$sip[$sip_line]['sip_display_name'] 			= $outbound_callerid_name;
					$sip[$sip_line]['sip_password'] 				= $sip_password;
					$sip[$sip_line]['sip_host'] 					= 'sbc1.feenix.co.nz';
					$sip[$sip_line]['sip_port'] 					= '5060';
					$sip[$sip_line]['route_id'] 					= $sip_id;
					$sip[$sip_line]['callerid'] 					= $outbound_callerid;
					$sip[$sip_line]['voicemail'] 					= $voicemail;
					$sip[$sip_line]['voicemail_password'] 			= $voicemail_password;
					$sip[$sip_line]['voicemail_email'] 				= $voicemail_email;
					
					$results[] = array(
							'accound_id' => $account_id,
							'account_name' => $account_name,
							'account_type' => $customer_type,
							'account_status' => $account_mode,
							'sip' => $sip,
							'phonenumber' => $phone
							);				
			}
				
			else{
						throw new HTTPException(
												'Unable to authenticate the account',
												402,
												array(
														'dev' => '102',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);	
			}

			return $results;
		
	}	
	
	
	public function availablePhoneNumber() {
		date_default_timezone_set('UTC');
		
		$request 		= $this->di->get('request');
		
		$did_prefix			= $request->get('phone_prefix', 'string');						
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$today				= date("Y-m-d H:i:s");
		$results			= array();
				
			
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$voice_db_user    = $this->di->getShared('config')->voice_test->username;
					$voice_db_pass    = $this->di->getShared('config')->voice_test->password;
					$voice_db_host    = $this->di->getShared('config')->voice_test->host;
					$voice_db     	= $this->di->getShared('config')->voice_test->dbname;
				}
				else {
					$voice_db_user    = $this->di->getShared('config')->voice->username;
					$voice_db_pass    = $this->di->getShared('config')->voice->password;
					$voice_db_host    = $this->di->getShared('config')->voice->host;
					$voice_db     	= $this->di->getShared('config')->voice->dbname;
				}
                $NewDBOBJ->sql_connect($voice_db_host,$voice_db_user,$voice_db_pass, $voice_db);
			

				
			$sql_did_result 		= $NewDBOBJ->query("SELECT * FROM dids WHERE provsioning_status='UNALLOCATED' AND did_prefix like '$did_prefix%' order by did");
		
			if( $NewDBOBJ->get_num_rows() > 0 ){

				while ($myphone = $NewDBOBJ->fetch_row($sql_did_result)) {
					$phone[$myphone['did']]['phone'] 		= $myphone['did'];
					$phone[$myphone['did']]['phone_prefix'] = $myphone['did_prefix'];
					$phone[$myphone['did']]['phone_location'] = $myphone['city'];
					$phone[$myphone['did']]['status'] = $myphone['provsioning_status'];
				}
					$results[] = array(
							'phonenumber' => $phone
							);									
				
			}
				
			else{
								throw new HTTPException(
														'Unable to find any phone number',
														431,
														array(
																'dev' => '131',
																'internalCode' => ''
																//'more' => '',
																//applicationCode => ''
															)
														);		
			}

			return $results;
		
	}	
	
	
	public function loginMyAccount() {
		
		date_default_timezone_set('UTC');
		$request 		= $this->di->get('request');
		$api_id			= $request->get('id', 'string');
		$account 		= $request->get('account', 'string');
		$sip_where 			= ( $request->get('sip_search', 'string')=='' ? '' : " AND ".$request->get('sip_search', 'string') );
		$phone_where 		= ( $request->get('phone_search', 'string')=='' ? '' : " AND ".$request->get('phone_search', 'string') );
		
				
		$validationObj 	= new \Feenix\Classes\Validation();
	
		$results 		= array();
		
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$voice_db_user    = $this->di->getShared('config')->voice_test->username;
					$voice_db_pass    = $this->di->getShared('config')->voice_test->password;
					$voice_db_host    = $this->di->getShared('config')->voice_test->host;
					$voice_db     	= $this->di->getShared('config')->voice_test->dbname;
				}
				else {
					$voice_db_user    = $this->di->getShared('config')->voice->username;
					$voice_db_pass    = $this->di->getShared('config')->voice->password;
					$voice_db_host    = $this->di->getShared('config')->voice->host;
					$voice_db     	= $this->di->getShared('config')->voice->dbname;
				}
                $NewDBOBJ->sql_connect($voice_db_host,$voice_db_user,$voice_db_pass, $voice_db);
				
			$sql_account 	 = "SELECT
						users.name,
						users.customer_type,
						users.account,
						users.account_mode,
						users.current_balance,
						users.id as user_id,
						users_settings.ratecard_id,
						users_settings.billing_type,
						users_settings.channels,
						users_settings.country_id,
						users_settings.dial_pattern_id,
						countries.name AS country_name,
						countries.dialcode,
						countries.iso,
						countries.nicename AS country_name_display,
						timezones.timezone_id,
						timezones.timezone,
						ratecards.ratecard_id,
						ratecards.ratecard_name
						FROM
						users
						Inner Join users_settings ON users.id = users_settings.user_id
						Inner Join countries ON users_settings.country_id = countries.country_id
						Inner Join timezones ON users_settings.timzone_id = timezones.timezone_id
						Inner Join ratecards ON users_settings.ratecard_id = ratecards.ratecard_id
						WHERE users.account='$account' ";

					
				
		$sql_account_result 		= $NewDBOBJ->query($sql_account);
		
			if( $NewDBOBJ->get_num_rows() > 0 ){
				
				
				while ($myaccount = $NewDBOBJ->fetch_row($sql_account_result)) {
					
					$account_id		= $myaccount['user_id'];
					$user_id		= $account_id;
					$account_name	= $myaccount['name'];
					$customer_type	= $myaccount['customer_type'];					
					$account_mode	= $myaccount['account_mode'];
					$ratecard_id	= $myaccount['ratecard_id'];
					$ratecard_name	= $myaccount['ratecard_name'];
					$channels		= $myaccount['channels'];
					$country_id		= $myaccount['country_id'];
					$country_name		= $myaccount['country_name_display'];
					$timezone_id		= $myaccount['timezone_id'];
					$timezone			= $myaccount['timezone'];
					
				}


		$sql_sip	 = "SELECT *,sipusers.id as sip_users_id,(select timezone from timezones where timezones.vm_timezone=voicemails.tz limit 1) as timezone,voicemails.fullname as voicemail_name,voicemails.mailbox as voicemail_box,voicemails.email as voicemail_email,voicemails.password as voicemail_password,voicemails.tz as voicemail_tz FROM sipusers Inner Join voicemails ON sipusers.name = voicemails.customer_id WHERE user_id='$user_id' $sip_where"; 
		$sql_did	 = "SELECT sipusers.id,sipusers.name as sip_line,did_routes.route_id,did_routes.did_route_type,users_dids.did_notes,dids.did,dids.city,dids.did_prefix,dids.provsioning_status from users_dids Inner Join dids ON users_dids.did_id = dids.id Inner Join did_routes ON dids.did=did_routes.did Inner Join sipusers ON did_routes.route_id=sipusers.id WHERE users_dids.user_id='$user_id' $phone_where ";
		$sql_sip_result		= $NewDBOBJ->query($sql_sip);		
		$sql_did_result		= $NewDBOBJ->query($sql_did);
				
				
				while ($myphone = $NewDBOBJ->fetch_row($sql_did_result)) {
					$phone[$myphone['did']]['phone'] = $myphone['did'];
					$phone[$myphone['did']]['phone_prefix'] = $myphone['did_prefix'];
					$phone[$myphone['did']]['phone_location'] = $myphone['city'];
					$phone[$myphone['did']]['phone_route_id'] = $myphone['route_id'];
					$phone[$myphone['did']]['phone_route_type'] = $myphone['did_route_type'];
					$phone[$myphone['did']]['phone_route_to_sip'] = $myphone['sip_line'];
					$phone[$myphone['did']]['notes'] = $myphone['did_notes'];
					$phone[$myphone['did']]['status'] = $myphone['provsioning_status'];
				}

				
				while ($mysip = $NewDBOBJ->fetch_row($sql_sip_result)) {
					$sip[$mysip['name']]['sip_line'] 					= $mysip['name'];
					$sip[$mysip['name']]['sip_auth_username'] 			= $mysip['username'];
					$sip[$mysip['name']]['sip_display_name'] 			= $mysip['voicemail_name'];
					$sip[$mysip['name']]['sip_password'] 				= $mysip['secret'];
					$sip[$mysip['name']]['sip_host'] 					= 'sbc1.feenix.co.nz';
					$sip[$mysip['name']]['sip_port'] 					= '5060';
					$sip[$mysip['name']]['route_id'] 					= $mysip['sip_users_id'];
					$sip[$mysip['name']]['callerid'] 					= $mysip['outbound_callerid'];
					$sip[$mysip['name']]['voicemail'] 					= $mysip['voicemail_box'];
					$sip[$mysip['name']]['voicemail_password'] 			= $mysip['voicemail_password'];
					$sip[$mysip['name']]['voicemail_email'] 			= $mysip['voicemail_email'];
					$sip[$mysip['name']]['voicemail_tz'] 				= $mysip['timezone'];
					$sip[$mysip['name']]['route_type']				    = $mysip['route_type'];
					$sip[$mysip['name']]['route_value']				    = $mysip['route_val'];
					$sip[$mysip['name']]['route_timeout']				= $mysip['route_timeout'];
					$sip[$mysip['name']]['route_phone_busy'] 			= $mysip['route_phone_busy'];
					$sip[$mysip['name']]['route_phone_noanswer'] 		= $mysip['route_phone_noanswer'];
					$sip[$mysip['name']]['route_phone_offline'] 		= $mysip['route_phone_offline'];
					$sip[$mysip['name']]['route_unconditional_type'] 	= $mysip['route_always_forward_type'];
					$sip[$mysip['name']]['route_unconditional_value'] 	= $mysip['route_always_forward_forward'];
				}	

					$results[] = array(
							'accound_id' => $account_id,
							'account_name' => $account_name,
							'account_type' => $customer_type,
							'account_status' => $account_mode,
							'ratecard_id' => $ratecard_id,
							'ratecard_name' => $ratecard_name,
							'channels' => $channels,
							'country_id' => $country_id,
							'country_name' => $country_name,
							'timezone_id' => $timezone_id,
							'timezone_name' => $timezone,
							'sip' => $sip,
							'phonenumber' => $phone
							);

			
			}
			
			else{
						throw new HTTPException(
												'Unable to authenticate the account',
												402,
												array(
														'dev' => '102',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);
			}
			
			return $results;		
	}

	
	public function MyCallRate() {		
		date_default_timezone_set('UTC');
		$request 		= $this->di->get('request');
		$api_id			= $request->get('id', 'string');
		$account 		= $request->get('account', 'string');
		$ratecard_id	= ( $request->get('ratecard_id', 'string')=='' ? "6" : $request->get('ratecard_id', 'string') );
		$country_search = ( $request->get('country', 'string')=='' ? " " :  " AND call_rate.country like '".$request->get('country', 'string').'%'."' ");
		
		$validationObj 	= new \Feenix\Classes\Validation();
	
		$results 		= array();
		
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$voice_db_user    = $this->di->getShared('config')->voice_test->username;
					$voice_db_pass    = $this->di->getShared('config')->voice_test->password;
					$voice_db_host    = $this->di->getShared('config')->voice_test->host;
					$voice_db     	= $this->di->getShared('config')->voice_test->dbname;
				}
				else {
					$voice_db_user    = $this->di->getShared('config')->voice->username;
					$voice_db_pass    = $this->di->getShared('config')->voice->password;
					$voice_db_host    = $this->di->getShared('config')->voice->host;
					$voice_db     	= $this->di->getShared('config')->voice->dbname;
				}
                $NewDBOBJ->sql_connect($voice_db_host,$voice_db_user,$voice_db_pass, $voice_db);

                        $sql_callrate   = "SELECT
                                                                call_rate.id,
																call_rate.country,
                                                                call_rate.provider_type,
                                                                call_rate.provider_prefix,
                                                                call_rate.orig_call_rate,
                                                                call_rate.call_setup_increment,
                                                                call_rate.call_setup_rate,
                                                                call_rate.call_rate_increment,
                                                                call_rate.call_rate,
                                                                call_rate.digit,
                                                                call_rate.ratecard_id,
                                                                call_rate.country_id,
                                                                call_rate.active,
                                                                countries.name,
                                                                countries.nicename,
                                                                countries.dialcode,
                                                                countries.iso,
                                                                countries.iso3,
                                                                countries.numcode
                                                                FROM
                                                                call_rate
                                                                Inner Join countries ON call_rate.country_id = countries.country_id
                                                                WHERE
                                                                call_rate.ratecard_id = '$ratecard_id' $country_search AND active='Y' ORDER BY country,provider_prefix limit 50000";


				$sql_call_rate_result		= $NewDBOBJ->query($sql_callrate);		
				
				while ($mycallrate = $NewDBOBJ->fetch_row($sql_call_rate_result)) {
						
						$callrate[$mycallrate['id']]['country'] 			  = $mycallrate['country'];
						$callrate[$mycallrate['id']]['prefix'] 			  	  = $mycallrate['provider_prefix'];
						$callrate[$mycallrate['id']]['prefix_type'] 			  = $mycallrate['provider_type'];
						$callrate[$mycallrate['id']]['ratecard_id'] 			  = $mycallrate['ratecard_id'];
						$callrate[$mycallrate['id']]['call_setup_increment'] 	= $mycallrate['call_setup_increment'];
						$callrate[$mycallrate['id']]['call_setup_rate'] 	  	= $mycallrate['call_setup_rate'];
						$callrate[$mycallrate['id']]['call_rate_increment']  	= $mycallrate['call_rate_increment'];
						$callrate[$mycallrate['id']]['call_rate'] 			  	= $mycallrate['call_rate'];
				}	
				
					$results[] = array(
							'ratecard' => $callrate
							);								
	
		return $results;			
	}
	

	public function MyAccountTimezone() {
		
		date_default_timezone_set('UTC');
		$request = $this->di->get('request');

		$validationObj 	= new \Feenix\Classes\Validation();
	
		$results 		= array();
		
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$voice_db_user    = $this->di->getShared('config')->voice_test->username;
					$voice_db_pass    = $this->di->getShared('config')->voice_test->password;
					$voice_db_host    = $this->di->getShared('config')->voice_test->host;
					$voice_db     	= $this->di->getShared('config')->voice_test->dbname;
				}
				else {
					$voice_db_user    = $this->di->getShared('config')->voice->username;
					$voice_db_pass    = $this->di->getShared('config')->voice->password;
					$voice_db_host    = $this->di->getShared('config')->voice->host;
					$voice_db     	= $this->di->getShared('config')->voice->dbname;
				}
                $NewDBOBJ->sql_connect($voice_db_host,$voice_db_user,$voice_db_pass, $voice_db);
				
		$search = ( $request->get('timezone', 'string')=='' ? " " :  " AND timezone like '".$request->get('timezone', 'string').'%'."' ");

		$sql_tz 	= "SELECT * FROM timezones WHERE vm_timezone != '' $country_search ORDER BY timezone";		

		$sql_result = $NewDBOBJ->query($sql_tz);

			if( $NewDBOBJ->get_num_rows() > 0 ){
				
				
				while ($tz = $NewDBOBJ->fetch_row()) {
					$results[] = array(
							'timezone_id' => $tz['timezone_id'],
							'timezone_name' => $tz['timezone'],
							'tz_gmt' => $tz['tz_gmt']
							);
				}
			
			}
			
			else{
						throw new HTTPException(
												'Unable to find the timezone',
												412,
												array(
														'dev' => '112',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);
			}
			
			return $results;		
	}




	public function MyAccountCDR() {

		date_default_timezone_set('UTC');
		$request = $this->di->get('request');

		//$username 		= $request->get('username', 'string');
		//$password 		= $request->get('password', 'string');
		$api_id			= $request->get('id', 'string');
		$start 			= $request->get('start_date', 'string');
		$end 			= $request->get('end_date', 'string');
		$account 		= $request->get('account', 'string');
		$call_type 		= $request->get('call_type', 'string');
		$dialed_no 		= $request->get('dialed_no', 'string');
		$timezone 		= ( $request->get('timezone', 'string')=='' ? 'Pacific/Auckland' : $request->get('timezone', 'string'));
				
		$validationObj 	= new \Feenix\Classes\Validation();
		//$password		= $validationObj->validateMyPassword($password);
		//$call_type		= $validationObj->validateCallType($call_type);
		$sip_users		= '';
		$results 			= array();
		//replace asterisk disposition to our own
		$disposition['NO ANSWER']	= 'No Answer';
		$disposition['CONGESTION']	= 'Unavailable';
		$disposition['FAILED']		= 'Failed';
		$disposition['BUSY']		= 'Busy';
		$disposition['ANSWERED']	= 'Answered';
		
		
			$sql_tz 	 		= "SELECT * FROM timezones WHERE timezone = '$timezone'";
			$sql_user_settings 	= "SELECT users.id,users.name,users.account,users.account_mode,users_settings.billing_accuracy FROM users Inner Join users_settings ON users_settings.user_id = users.id WHERE users.account = '$account' AND users.active='Y'";
			
			$sql_cdr_order	= " ORDER BY calldate ";
			
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$cdr_db_user    = $this->di->getShared('config')->cdr_test->username;
					$cdr_db_pass    = $this->di->getShared('config')->cdr_test->password;
					$cdr_db_host    = $this->di->getShared('config')->cdr_test->host;
					$cdr_db     	= $this->di->getShared('config')->cdr_test->dbname;
				}
				else {
					$cdr_db_user    = $this->di->getShared('config')->cdr->username;
					$cdr_db_pass    = $this->di->getShared('config')->cdr->password;
					$cdr_db_host    = $this->di->getShared('config')->cdr->host;
					$cdr_db     	= $this->di->getShared('config')->cdr->dbname;
				}
                $NewDBOBJ->sql_connect($cdr_db_host,$cdr_db_user,$cdr_db_pass, $cdr_db);
				
		$sql_tz_result = $NewDBOBJ->query("$sql_tz");
		
		if( empty($sql_tz_result) ){

						throw new HTTPException(
												'Unable to locate the timezone',
												414,
												array(
														'dev' => '114',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);		
					return $results;											
			
		}			
		
			//get the user settings 
			$sql_user_settings_result = $NewDBOBJ->query($sql_user_settings);
			
			
			while ($user_settings = $NewDBOBJ->fetch_row($sql_user_settings_result)) {
						$master_account	= $user_settings['account'];
						$account_name	= $user_settings['name']; 
						$account_id		= $user_settings['id'];
						$billing_accuracy= $user_settings['billing_accuracy'];
			}	
			
			//now get all the sip accounts of the user
			$sql_sip_user_result = $NewDBOBJ->query("SELECT
														users.name,
														sipusers.name as sip_name,
														users.account
														FROM
														users
														Inner Join sipusers ON users.id = sipusers.user_id
														WHERE 
														users.account='$account'");
			
			
			while ($sip_user_settings = $NewDBOBJ->fetch_row($sql_sip_user_result)) {
						//$sip_name[]	= $sip_user_settings['name']; 
						$sip_users	.= " '".$sip_user_settings['sip_name']."',";
			}
						$sip_users	 = rtrim($sip_users,",");

			if( empty($sip_users) ) {

						throw new HTTPException(
												'Unable to find the account',
												428,
												array(
														'dev' => '128',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);		
					return $results;	
				
			}
			

			if($dialed_no!='')
			$sql_cdr_dialed_where	= " AND dst like '$dialed_no%' ";
				
			if( empty($start) ) {
				$start = date('Y-m-d H:i:s', strtotime("-7 day"));
				$end   = date('Y-m-d H:i:s');
			}
			
			$sql_cdr 	 	= "SELECT id,calldate,end,uniqueid,calldate as mycalldate,end as myenddate,clid,src,country_code,billed_prefix,ddi,dst,billed_no,src_ip,duration,billsec,cdr_type,disposition,account_user,account,ratecard_id,rate,rate_total,country,SUBSTRING_INDEX(dstchannel,'-','1') AS location FROM cdr WHERE end BETWEEN  '$start' and '$end' 
								AND account in ('$account') $sql_cdr_user_where   $sql_cdr_dialed_where";		
	
			
						
		$sql_result_cdr = $NewDBOBJ->query($sql_cdr);
		//$results[] = "$sql_cdr";
		
			if( !empty($sql_result_cdr) ){
				
				
				while ( $cdr = $NewDBOBJ->fetch_row($sql_result_cdr) ) {
										
						if($cdr['billed_no']=='') {
							$billed_no = $cdr['ddi'];	
						} 
						else {
							$billed_no = $cdr['billed_no'];
						}
						if (preg_match_all("~\<\s*(.*?)\s*\>~", $cdr['clid'], $cli1)) {
							$clid 		=	$cli1[1][0];
						}
						if (preg_match_all('~\"\s*(.*?)\s*\"~', $cdr['clid'], $cli2)){
							$clid_name 	=	$cli2[1][0];
						}
					$cdr_end_date = date("Y-m-d H:i:s", (strtotime(date($cdr['calldate'])) + ($cdr['duration'] + $cdr['billsec'])));	
					$results[] = array(
							'calldate' => $cdr['calldate'],
							'enddate' => $cdr_end_date,
							'callerid' => $clid,
							'callerid_name' => $clid_name,
							'caller' => $cdr['src'],
							'callee' => $billed_no,
							'src_ip' => $cdr['src_ip'],
							'duration' => ceil($cdr['billsec'] * (pow(10,0))) / pow(10,0),
							'call_status' => $disposition[$cdr['disposition']],
							'call_uid' => $cdr['id'],
							'call_country' => $cdr['country'],
							'country_code'  => $cdr['country_code'],
							'call_prefix'  => $cdr['billed_prefix'],
							'account' => $cdr['account'],
							'account_user' => $cdr['account_user'],
							'call_rate' => $cdr['rate'],
							'total_rate' => $cdr['rate_total'],
							'location' => $cdr['location'],
							'cdr_type' => $cdr['cdr_type']
							);
							unset($cli1);
							unset($cli2);
							$clid='';
							$clid_name='';
							$cdr_end_date='';
				}
			
			}
			
			else{
						throw new HTTPException(
												'Unable to find any call record',
												415,
												array(
														'dev' => '115',
														'internalCode' => ''
														//'more' => '',
														//applicationCode => ''
													)
												);
			}
			
			return $results;		
	}


        public function radclientSubscriber() {

                //date_default_timezone_set('UTC');

                $validationObj  = new \Feenix\Classes\Validation();

                $request                        = $this->di->get('request');
                $api_id                         = $request->get('id', 'string');
                $username                       = $request->get('username', 'string');
                $event_type                     = $validationObj->validateradclientEventType($request->get('event_type', 'string')); //disconnect or coa
                $event_data       		= $request->get('event_data') ;
                $event_data       		= json_decode($event_data, true);
                $desc                           = $request->get('desc', 'string');
		$event_response			= '';
		$extra_command			= '';

                $NewDBOBJ       = new \Feenix\Classes\MySQLDB();
                                if( preg_match("/-test$/",$api_id) ) {
                                        $rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
                                        $rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
                                        $rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
                                        $rad_db         = $this->di->getShared('config')->radius_test->dbname;
                                        $setup_date             = date($this->di->getShared('config')->radius->dformat);
                                }
                                else {
                                        $rad_db_user    = $this->di->getShared('config')->radius->db_username;
                                        $rad_db_pass    = $this->di->getShared('config')->radius->db_password;
                                        $rad_db_host    = $this->di->getShared('config')->radius->db_host;
                                        $rad_db         = $this->di->getShared('config')->radius->dbname;
                                        $setup_date             = date($this->di->getShared('config')->radius->dformat);
                                }

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);
                   foreach ($event_data as $p => $pval) {

                            $sql_event_log  .= "$p=$pval".",";
			    $event_count++;
                   }


                if( empty($username) ) {

                                                throw new HTTPException(
                                                                                                'Unable to verify the subscriber name',
                                                                                                412,
                                                                                                array(
                                                                                                                'dev' => '412',
                                                                                                                'internalCode' => '',
                                                                                                                'more' => 'Subscriber username is invalid'
                                                                                                                //applicationCode => ''
                                                                                                        )
                                                                                                );
                }

/*
change the profile
echo "User-Name=servian@feenix.co.nz,Alc-Subsc-ID-Str=servian@feenix.co.nz,Alc-Subsc-Prof-Str=sub-profile-1" | /usr/local/bin/radclient 10.2.49.21:3799 coa 7ooETFBKsF9YTcTh
			
ssh and use sudo to run a command
echo 03455295042p | sudo -S echo "User-Name=servian@feenix.co.nz,Alc-Subsc-ID-Str=servian@feenix.co.nz" | /usr/local/bin/radclient 10.2.49.21:3799 $event_type 7ooETFBKsF9YTcTh
*/

				$sql_event_log = rtrim($sql_event_log,",");

				if( $sql_event_log ) {

					$extra_command = ",".$sql_event_log; 
				}

		$command = 'echo 123456 | sudo -S echo "User-Name='.$username.',Alc-Subsc-ID-Str='."$username$extra_command".'" | /usr/local/bin/radclient 10.2.49.21:3799 '.$event_type.' 7ooETFBKsF9YTcTh';
		$command_response = "User-Name=$username".",Alc-Subsc-ID-Str=$username$extra_command";
$ssh_user='japi';
$ssh_pass='123456';

$nas = array('10.2.49.20','10.2.49.21','10.1.49.20','10.1.49.21');

foreach ($nas as $nas_ip) {
$command = 'echo 123456 | sudo -S echo "User-Name='.$username.',Alc-Subsc-ID-Str='."$username$extra_command".'" | /usr/local/bin/radclient '.$nas_ip.':3799 '.$event_type.' 7ooETFBKsF9YTcTh';
$connection = \ssh2_connect('srv1', 22);
ssh2_auth_password($connection, "$ssh_user", "$ssh_pass");
$stream = \ssh2_exec($connection, "$command");
\stream_set_blocking($stream, true);
$stream_out = \ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
$stream_content[$nas_ip] =  stream_get_contents($stream_out);
                                       
}                                        
					if( $event_type!='disconnect' && empty($sql_event_log) ) 
					 {
                                                throw new HTTPException(
                                                                                                'Unable to initiate the request',
                                                                                                427,
                                                                                                array(
                                                                                                                'dev' => '427',
                                                                                                                'internalCode' => '',
                                                                                                                'more' => 'Your event_data is not valid, please check your request'
                                                                                                                //applicationCode => ''
                                                                                                        )
                                                                                                );                                                           
                                        }

                                                $results[] = array(
								'username' => $username,
                                                                'event_type' => $event_type,
                                                                'extra_command' => "$command_response",
                                                                'desc' => "$desc",
                                                                'event_response' => $stream_content
                                                                );

                                return $results;

        }


	public function createProfile() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');
		$profile			= $request->get('profile', 'string');	
		$profile_data       = $request->get('profile_data') ;
		$profile_data       = json_decode($profile_data, true);		
		$desc				= $request->get('desc', 'string');

                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date		= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date		= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);


                $rad_user_profile = $NewDBOBJ->query("SELECT count(*) as total_profile_param FROM radgroupreply WHERE groupname='$profile'");

                        while ($rad_user_profile_data   = $NewDBOBJ->fetch_row($rad_user_profile)) {
                                $radprfoileparam     	=  $rad_user_profile_data['total_profile_param'];
                        }

				if( $radprfoileparam > 1 ) {
						throw new HTTPException(
												'Profile already exist',
												423,
												array(
														'dev' => '423',
														'internalCode' => '',
														'more' => 'Subscriber profile already exist, please use different profile name'
														//applicationCode => ''
													)
												);						
				}
				
				if( count($profile_data) < 1 ) {
						throw new HTTPException(
												'Profile should have paramters',
												424,
												array(
														'dev' => '424',
														'internalCode' => '',
														'more' => 'Subscriber profile does not have any value, profile need atleast one parameter'
														//applicationCode => ''
													)
												);						
				}
				$NewDBOBJ->query("insert into radprofile(profile,`desc`) values('$profile','$desc')");
				//now clean the profile first.
				$NewDBOBJ->query("delete from radgroupreply where groupname='$profile'");
                   foreach ($profile_data as $p => $pval) {
                            $sql_profile_log  = $NewDBOBJ->query("INSERT INTO radgroupreply(groupname,`attribute`,`op`,`value`) values('$profile','$p',':=','$pval')");
                   }

					if( $sql_profile_log ) {
						$results[] = array(						
								'profile' => $profile,
								'profile_data' => $profile_data,
								'desc' => "$desc",
								'setup_date' => $setup_date
								);
					} 
					else {
						throw new HTTPException(
												'Unable to setup the subscriber',
												414,
												array(
														'dev' => '414',
														'internalCode' => '',
														'more' => 'Subscriber cant be added, please check your username and password'
														//applicationCode => ''
													)
												);									
					}
		
				return $results;
			
	}

	public function deleteProfile() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');		
		$profile			= $request->get('profile', 'string');	

                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);


                $rad_user_profile = $NewDBOBJ->query("SELECT count(*) as total_profile_param FROM radgroupreply WHERE groupname='$profile'");

                        while ($rad_user_profile_data   = $NewDBOBJ->fetch_row($rad_user_profile)) {
                                $radprfoileparam     	=  $rad_user_profile_data['total_profile_param'];
                        }

				if( $radprfoileparam < 1 ) {
						throw new HTTPException(
												'Unable to find the profile',
												419,
												array(
														'dev' => '419',
														'internalCode' => '',
														'more' => 'Subscriber profile does not exist'
														//applicationCode => ''
													)
												);					
				}
				
				$NewDBOBJ->query("delete from radprofile where profile='$profile'");
				//now clean the profile.
				$NewDBOBJ->query("delete from radgroupreply where groupname='$profile'");
                
						$results[] = array(						
								'profile' => $profile,
								'msg' => "Profile has been deleted"
								);
					
		
				return $results;
			
	}

	public function updateProfile() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');		
		$profile			= $request->get('profile', 'string');	
		$profile_data       = $request->get('profile_data') ;
		$profile_data       = json_decode($profile_data, true);		
		$desc				= $request->get('desc', 'string');

                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);


                $rad_user_profile = $NewDBOBJ->query("SELECT count(*) as total_profile_param FROM radgroupreply WHERE groupname='$profile'");

                        while ($rad_user_profile_data   = $NewDBOBJ->fetch_row($rad_user_profile)) {
                                $radprfoileparam     	=  $rad_user_profile_data['total_profile_param'];
                        }

				if( $radprfoileparam < 1 ) {
						throw new HTTPException(
												'Unable to find the profile',
												419,
												array(
														'dev' => '419',
														'internalCode' => '',
														'more' => 'Subscriber profile does not exist'
														//applicationCode => ''
													)
												);					
				}
				
				if( count($profile_data) < 1 ) {
						throw new HTTPException(
												'Profile should have paramters',
												424,
												array(
														'dev' => '424',
														'internalCode' => '',
														'more' => 'Subscriber profile does not have any value, profile need atleast one parameter'
														//applicationCode => ''
													)
												);						
				}
				//now clean the profile first.
				$NewDBOBJ->query("delete from radgroupreply where groupname='$profile'");
                   foreach ($profile_data as $p => $pval) {
                            $sql_profile_log  = $NewDBOBJ->query("INSERT INTO radgroupreply(groupname,`attribute`,`op`,`value`) values('$profile','$p',':=','$pval')");
                   }

					if( $sql_profile_log ) {
						$results[] = array(						
								'profile' => $profile,
								'profile_data' => $profile_data,
								'desc' => "$desc"
								);
					} 
					else {
						throw new HTTPException(
												'Unable to setup the subscriber',
												414,
												array(
														'dev' => '414',
														'internalCode' => '',
														'more' => 'Subscriber cant be added, please check your username and password'
														//applicationCode => ''
													)
												);									
					}
		
				return $results;
			
	}

	public function createSubscriber() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');
		$username 			= $request->get('username', 'string');
		$password 			= $validationObj->validateMyPassword($request->get('password', 'string'));
		$default_profile	= "DEFAULT";	
		$default_cgnat		= "yes";	
		$radiusServiceType 	= $request->get('radiusServiceType', 'string');
		$proxy 				= ( $request->get('proxy', 'string')=='' ? "LOCAL" : $request->get('proxy', 'string') );
		$framedPool 		= $request->get('framedPool', 'string');
		$clientDNSPri 		= $request->get('clientDNSPri', 'string');
		$portBaseAuth		= $validationObj->validatebolean($request->get('portBaseAuth', 'string'));//yes or no
		$portBaseParameter	= $request->get('portBaseParameter', 'string');//parameter
		$portBaseValue		= $request->get('portBaseValue', 'string');//parameter
		$cgnat				= $validationObj->validatebolean($request->get('cgnat', 'string'));//yes or no
		$profile 			= ( $request->get('profile', 'string')=='' ? "$default_profile" : $request->get('profile', 'string') );
		$extra				= $request->get('extra');
		$extra				= json_decode($extra, true);

		
		$desc				= $request->get('desc', 'string');
		$default_realm		= "feenix.co.nz";
		$seperator			= "@";		
		$parse_username_array = explode("$seperator",$username);
		$assigned_date		  = date("Y-m-d H:i:s");

		if( count($parse_username_array) >2 ) {
			
						throw new HTTPException(
												'Unable to verify the subscriber name',
												412,
												array(
														'dev' => '412',
														'internalCode' => '',
														'more' => 'Subscriber username is invalid'
														//applicationCode => ''
													)
												);				
		}
	
/*	
		if( count($parse_username_array) == 1 ) {			
			$realm 		= "$default_realm";
			//$username	= preg_replace("/^'|[^A-Za-z0-9\'-]|'$/", '', $username)."$seperator"."$realm";
			$username	= "$username$seperator$default_realm";
		}
		
		if( count($parse_username_array) == 2 ) {			
			$realm 		= $parse_username_array[1];
			//$username	= preg_replace("/^'|[^A-Za-z0-9\'-]|'$/", '', $parse_username_array[0])."$seperator"."$realm";
			$username	= $parse_username_array[0]."$seperator$realm";
		}
	
*/				
                
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);


                $rad_user_profile = $NewDBOBJ->query("SELECT count(*) as total_profile_param FROM radgroupreply WHERE groupname='$profile'");

                        while ($rad_user_profile_data   = $NewDBOBJ->fetch_row($rad_user_profile)) {
                                $radprfoileparam     	=  $rad_user_profile_data['total_profile_param'];
                        }

				if( $radprfoileparam < 1 ) {
						throw new HTTPException(
												'Unable to find the profile',
												419,
												array(
														'dev' => '419',
														'internalCode' => '',
														'more' => 'Subscriber profile does not exist'
														//applicationCode => ''
													)
												);						
				}


                $rad_user_proxy = $NewDBOBJ->query("SELECT count(*) as total_proxy_param FROM realm WHERE realm='$proxy' and actve='Y'");

                        while ($rad_user_proxy_data   = $NewDBOBJ->fetch_row($rad_user_proxy)) {
                                $radproxy     	=  $rad_user_proxy_data['total_proxy_param'];
                        }

				if( $radproxy < 1 ) {
						throw new HTTPException(
												'Unable to find the proxy information',
												425,
												array(
														'dev' => '425',
														'internalCode' => '',
														'more' => 'Proxy is not setup on radius'
														//applicationCode => ''
													)
												);						
				}


                $rad_user = $NewDBOBJ->query("SELECT * FROM radcheck WHERE username='$username'");

                        while ($rad_user_data   = $NewDBOBJ->fetch_row($rad_user)) {
                                $raduser     	=  $rad_user_data['username'];
                        }
		
				if( !empty($raduser) ) {		

						throw new HTTPException(
												'Unable to setup the subscriber',
												410,
												array(
														'dev' => '410',
														'internalCode' => '',
														'more' => 'Subscriber already exists'
														//applicationCode => ''
													)
												);					
				}
			
			
				else {
					
					//now check if its port based authenticaiton 
					if( $portBaseAuth=="YES" ) {
							if( empty($portBaseValue) ) {
								throw new HTTPException(
														'Unable to read the port value',
														413,
														array(
																'dev' => '413',
																'internalCode' => '',
																'more' => 'Subscriber Port should have a value if the port based authentication is enabled'
																//applicationCode => ''
															)
														);								
							}
							//else just add the port
							$NewDBOBJ->query("INSERT INTO radportuser(`username`,`port`,`port_value`) values('$username','$portBaseParameter','$portBaseValue')");
					}
					
					$NewDBOBJ->query("INSERT INTO radcheck(`username`,`attribute`,`value`,`desc`,`realm`) values('$username','Cleartext-Password','$password','$desc','$proxy')");
					$rad_id = $NewDBOBJ->sql_id();
					
					if( $cgnat == "YES" ) { 
						$NewDBOBJ->query("INSERT INTO radusergroup(`username`,`groupname`) values('$username','$profile')");
						$NewDBOBJ->query("DELETE FROM radreply WHERE username='$username'");
						$NewDBOBJ->query("UPDATE radstaticip SET is_assigned='N',username='' WHERE username='$username'");
					} else {
						
						$NewDBOBJ->query("INSERT INTO radusergroup(`username`,`groupname`) values('$username','$profile')");
						//the below code is only for nova.(local db for static ips)
						//this means now you need to assign a static ip
                		$rad_static_ip = $NewDBOBJ->query("SELECT * FROM radstaticip where is_assigned='N' order by id desc limit 1");
                        while ($rad_static_ip_data   = $NewDBOBJ->fetch_row($rad_static_ip)) {
                                $static_ip     		 =  $rad_static_ip_data['static_ip'];
								$static_ipv6     	 =  $rad_static_ip_data['static_ipv6'];
                        }			
						if( !empty($static_ip) ) {			
							$NewDBOBJ->query("DELETE FROM radusergroup WHERE username='$username'");
							$NewDBOBJ->query("INSERT INTO radusergroup(`username`,`groupname`) values('$username','static-ip-group')");
							$NewDBOBJ->query("DELETE FROM radreply WHERE username='$username'");
							$NewDBOBJ->query("INSERT INTO radreply(`username`,`attribute`,`op`,`value`) values('$username','Framed-IP-Address',':=','$static_ip')");
							$NewDBOBJ->query("INSERT INTO radreply(`username`,`attribute`,`op`,`value`) values('$username','Delegated-IPv6-Prefix',':=','$static_ipv6')");
							$NewDBOBJ->query("UPDATE radstaticip SET is_assigned='Y',username='$username',assigned_date='$assigned_date' WHERE static_ip='$static_ip'");
												
						}
							
					}
					
					//if extra have smething
					if( count($extra) > 0 ) {
						$NewDBOBJ->query("delete from radreply where username='$username'");
						   foreach ($extra as $p => $pval) {
									$NewDBOBJ->query("INSERT INTO radreply(username,`attribute`,`op`,`value`) values('$username','$p',':=','$pval')");
						   }	
					}
					
					if( $rad_id ) {
						$results[] = array(
								'username' => $username,
								'password' => $password,
								'radiusID' => $rad_id,
								'profile' => $profile,
								'portBaseAuth' => $portBaseAuth ,
								'portBaseParameter' => $portBaseParamter ,
								'portBaseValue' => $portBaseValue ,
								'realm' => $realm,
								'extra' => "$extra",
								'CGNAT' => $cgnat,
								'ip_address' => $static_ip,
								'desc' => "$desc",
								'setup_date' => $setup_date
								);
					} 
					else {
						throw new HTTPException(
												'Unable to setup the subscriber',
												414,
												array(
														'dev' => '414',
														'internalCode' => '',
														'more' => 'Subscriber cant be added, please check your username and password'
														//applicationCode => ''
													)
												);									
					}
				}
			
			
			return $results;				
				

		}


	public function renameSubscriber() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');
		$username 			= $request->get('username', 'string');
		$new_username 			= $request->get('new_username', 'string');
		$default_profile	= "DEFAULT";	

		$desc				= $request->get('desc', 'string');
		$default_realm		= "feenix.co.nz";
		$seperator			= "@";		
		$parse_username_array = explode("$seperator",$username);
		$assigned_date		  = date("Y-m-d H:i:s");

		if( count($parse_username_array) >2 ) {
			
						throw new HTTPException(
												'Unable to verify the subscriber name',
												412,
												array(
														'dev' => '412',
														'internalCode' => '',
														'more' => 'Subscriber username is invalid'
														//applicationCode => ''
													)
												);				
		}
		
		if( count($parse_username_array) == 1 ) {			
			$realm 		= "$default_realm";
			//$username	= preg_replace("/^'|[^A-Za-z0-9\'-]|'$/", '', $username)."$seperator"."$realm";
			$username	= "$username$seperator$default_realm";
		}
		
		if( count($parse_username_array) == 2 ) {			
			$realm 		= $parse_username_array[1];
			//$username	= preg_replace("/^'|[^A-Za-z0-9\'-]|'$/", '', $parse_username_array[0])."$seperator"."$realm";
			$username	= $parse_username_array[0]."$seperator$realm";
		}
	
				
                
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);


                $rad_user_login = $NewDBOBJ->query("SELECT * from radcheck where username='$username'");

                        while ($rad_user_profile_data   = $NewDBOBJ->fetch_row($rad_user_login)) {
                                $radusername     	=  $rad_user_profile_data['username'];
								$radpassword     	=  $rad_user_profile_data['value'];
                        }

				if( empty($radusername) ) {
						throw new HTTPException(
												'Unable to verify the subscriber name',
												412,
												array(
														'dev' => '412',
														'internalCode' => '',
														'more' => 'Subscriber username is invalid or does not exist'
														//applicationCode => ''
													)
												);						
				}

				if( empty($new_username) ) {
						throw new HTTPException(
												'Unable to verify the new subscriber name',
												421,
												array(
														'dev' => '421',
														'internalCode' => '',
														'more' => 'New Subscriber username is invalid or does not exist'
														//applicationCode => ''
													)
												);						
				}

						$NewDBOBJ->query("update radcheck set username='$new_username' where username='$username'");
						$NewDBOBJ->query("update radportuser set username='$new_username' where username='$username'");
						$NewDBOBJ->query("update radreply set username='$new_username' where username='$username'");
						$NewDBOBJ->query("update radreply set `value`='$new_username' where `value`='$username'");
						$NewDBOBJ->query("update radusergroup set username='$new_username' where username='$username'");	
				
					
						$results[] = array(
								'username' => $username,
								'password' => $radpassword,
								'msg' => "Subscriber has been renamed",
								'desc' => "$desc"
								);
								
			return $results;				
				

		}


	public function deleteSubscriber() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');
		$username 			= $request->get('username', 'string');
		$default_realm		= "feenix.co.nz";
		$seperator			= "@";	

                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);

                $rad_user = $NewDBOBJ->query("SELECT * FROM radcheck WHERE username='$username'");

                        while ($rad_user_data   = $NewDBOBJ->fetch_row($rad_user)) {
                                $raduser     	=  $rad_user_data['username'];
                        }
		
				if( empty($raduser) ) {		

						throw new HTTPException(
												'Unable to find the subscriber',
												416,
												array(
														'dev' => '416',
														'internalCode' => '',
														'more' => 'Subscriber does not exist'
														//applicationCode => ''
													)
												);					
				}	

                $NewDBOBJ->query("delete FROM radcheck WHERE username = '$username' ");
				$NewDBOBJ->query("delete FROM radusergroup WHERE username = '$username' ");
                $NewDBOBJ->query("delete FROM radportuser WHERE username = '$username' "); 
				$NewDBOBJ->query("delete FROM radreply WHERE username = '$username' ");        
				$NewDBOBJ->query("UPDATE radstaticip SET username='' , is_assigned='N' , assigned_date='',notes='' WHERE username = '$username' ");	
							

						$results[] = array(
								'username' => $username,
								'msg' => 'Subscriber has been deleted'
								);			
			
			return $results;		
		
		
	}

	

	public function listSubscriber() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');
		$username 			= $request->get('username', 'string');
		$default_realm		= "feenix.co.nz";
		$seperator			= "@";	

                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				
                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);

                //$rad_users = $NewDBOBJ->query("SELECT * FROM radcheck WHERE username like '%$default_realm' ORDER BY id asc");
				$rad_users = $NewDBOBJ->query("SELECT
radcheck.id,
radcheck.username,
radcheck.attribute,
radcheck.op,
radcheck.value,
radusergroup.groupname
FROM
radcheck
Inner Join radusergroup ON radcheck.username = radusergroup.username ORDER BY radcheck.username asc");
                        while ($rad_user_data     = $NewDBOBJ->fetch_row($rad_users)) {
                                
								$raddata[$rad_user_data['id']]['username'] =  $rad_user_data['username'];
								$raddata[$rad_user_data['id']]['password'] =  $rad_user_data['value'];
								$raddata[$rad_user_data['id']]['profile'] =  $rad_user_data['groupname'];
								$raddata[$rad_user_data['id']]['desc']     =  $rad_user_data['desc'];
                        		
						}
								

						$results[] = array(
								'subscribers' => $raddata
								);			
			
			return $results;		
		
		
	}

	public function listStaticIPs() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');
		$raddata			= '';
		$results			= array();

                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);

                $rad_ip = $NewDBOBJ->query("SELECT * FROM radstaticip ORDER BY id asc");

                        while ($rad_ip_data     = $NewDBOBJ->fetch_row($rad_ip)) {
                                
								$raddata[$rad_ip_data['id']]['ipv4'] 		=  $rad_ip_data['static_ip'];
								$raddata[$rad_ip_data['id']]['ipv6'] 		=  $rad_ip_data['static_ipv6'];
								$raddata[$rad_ip_data['id']]['assigned'] 	=  $rad_ip_data['is_assigned'];
                        		$raddata[$rad_ip_data['id']]['subscriber'] 	=  $rad_ip_data['username'];
						}
								

						$results[] = array(
								'subscribers' => $raddata
								);			
			
			return $results;		
		
		
	}

	public function updateSubscriber() {
		
		//date_default_timezone_set('UTC');

		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 			= $this->di->get('request');
		$api_id				= $request->get('id', 'string');
		$username 			= $request->get('username', 'string');
		$password 			=	$request->get('password', 'string');
		$default_profile	= "DEFAULT";	
		$default_cgnat		= "yes";		
		$radiusServiceType 	= $request->get('radiusServiceType', 'string');
		$ip_address 		= $request->get('ip_address', 'string');
		//enable proxy line later
		$proxy				= ( $request->get('proxy', 'string')=='' ? 'LOCAL' : $request->get('proxy', 'string') );		
		$framedPool 		= $request->get('framedPool', 'string');
		$clientDNSPri 		= $request->get('clientDNSPri', 'string');
		$profile 			= $request->get('profile', 'string');
		$portBaseAuth		= $validationObj->validatebolean($request->get('portBaseAuth', 'string'));//yes or no
		$portBaseParameter	= ( $request->get('portBaseParameter', 'string')=='' ? 'ASID' : $request->get('portBaseParameter', 'string') );//parameter
		$portBaseValue		= $request->get('portBaseValue', 'string');//parameter
		$desc 				= $request->get('desc', 'string');
		$cgnat				= $validationObj->validatebolean($request->get('cgnat', 'string'));//yes or no
		$extra				= $request->get('extra');
		$extra				= json_decode($extra, true);
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);


                $rad_user = $NewDBOBJ->query("SELECT * FROM radcheck WHERE username='$username'");

                        while ($rad_user_data   = $NewDBOBJ->fetch_row($rad_user)) {
                                $raduser     	=  $rad_user_data['username'];
                        }
		
				if( empty($raduser) ) {		

						throw new HTTPException(
												'Unable to find the subscriber',
												416,
												array(
														'dev' => '416',
														'internalCode' => '',
														'more' => 'Subscriber does not exist'
														//applicationCode => ''
													)
												);					
				}			

                $rad_user_proxy = $NewDBOBJ->query("SELECT count(*) as total_proxy_param FROM realm WHERE realm='$proxy' and actve='Y'");

                        while ($rad_user_proxy_data   = $NewDBOBJ->fetch_row($rad_user_proxy)) {
                                $radproxy     	=  $rad_user_proxy_data['total_proxy_param'];
                        }

				if( $radproxy < 1 ) {
						throw new HTTPException(
												'Unable to find the proxy information',
												425,
												array(
														'dev' => '425',
														'internalCode' => '',
														'more' => 'Proxy is not setup on radius'
														//applicationCode => ''
													)
												);						
				}
	
				
                $rad_user_profile = $NewDBOBJ->query("SELECT count(*) as total_profile_param FROM radgroupreply WHERE groupname='$profile'");

                        while ($rad_user_profile_data   = $NewDBOBJ->fetch_row($rad_user_profile)) {
                                $radprfoileparam     	=  $rad_user_profile_data['total_profile_param'];
                        }

				//cant find the profile
				if( $radprfoileparam < 1 ) {
						throw new HTTPException(
												'Unable to find the profile',
												419,
												array(
														'dev' => '419',
														'internalCode' => '',
														'more' => 'Subscriber profile does not exist'
														//applicationCode => ''
													)
												);						
				}

					//disable proxy function untill daniel integrate with portal
					//$NewDBOBJ->query("update radcheck set `value`='$password',`desc`='$desc',`realm`='$proxy' WHERE username='$username'");
					$NewDBOBJ->query("update radcheck set `value`='$password',`desc`='$desc' WHERE username='$username'");
					
					//update the profile
					$NewDBOBJ->query("UPDATE radusergroup SET groupname='$profile' WHERE username='$username'");
					//now check if its port based authenticaiton 
					if( $portBaseAuth=="YES" ) {
							if( empty($portBaseValue) ) {
								throw new HTTPException(
														'Unable to read the port value',
														413,
														array(
																'dev' => '413',
																'internalCode' => '',
																'more' => 'Subscriber Port should have a value if the port based authentication is enabled'
																//applicationCode => ''
															)
														);								
							}
							$NewDBOBJ->query("DELETE FROM radportuser WHERE username='$username'");
							//just add the port
							$NewDBOBJ->query("INSERT INTO radportuser(`username`,`port`,`port_value`) values('$username','$portBaseParameter','$portBaseValue')");
					}


				
				if( $cgnat == "YES" ) {
						$NewDBOBJ->query("DELETE FROM radreply WHERE username='$username'");
						$NewDBOBJ->query("UPDATE radstaticip SET is_assigned='N',username='' WHERE username='$username'");
				} else {
						//this means now you need to assign a static ip
                		$rad_static_ip = $NewDBOBJ->query("SELECT * FROM radstaticip where is_assigned='N' order by id asc limit 1");
                        while ($rad_static_ip_data   = $NewDBOBJ->fetch_row($rad_static_ip)) {
                                $static_ip     		 =  $rad_static_ip_data['static_ip'];
								$static_ipv6		 =  $rad_static_ip_data['static_ipv6'];	
                        }			
						if( !empty($static_ip) ) {			
							$NewDBOBJ->query("UPDATE radusergroup SET `groupname`='static-ip-group' WHERE username='$username'");
							$NewDBOBJ->query("DELETE FROM radreply WHERE username='$username'");
							$NewDBOBJ->query("INSERT INTO radreply(`username`,`attribute`,`op`,`value`) values('$username','Framed-IP-Address',':=','$static_ip')");
							$NewDBOBJ->query("INSERT INTO radreply(`username`,`attribute`,`op`,`value`) values('$username','Delegated-IPv6-Prefix',':=','$static_ipv6')");
							$NewDBOBJ->query("UPDATE radstaticip SET is_assigned='Y',username='$username',assigned_date='$assigned_date' WHERE static_ip='$static_ip'");
												
						}
				}
				
				

					$no_of_extra =count($extra); 
					if( $no_of_extra > 0 ) {
						$NewDBOBJ->query("delete from radreply where username='$username'");
						   foreach ($extra as $p => $pval) {
							   		$pval_data = explode(",",$pval);
									if(count($pval_data) == 1) {
										$NewDBOBJ->query("INSERT INTO radreply(username,`attribute`,`op`,`value`) values('$username','$p',':=','$pval')");
									} else {
										for($pmval=0;$pmval<count($pval_data);$pmval++) {
											$NewDBOBJ->query("INSERT INTO radreply(username,`attribute`,`op`,`value`) values('$username','$p',':=','".$pval_data[$pmval]."')");	
										}
										unset($pval_data);//unset now
									}
						   }	
					}
					
						$results[] = array(
								'username' => $username,
								'password' => $password,
								'profile' => $profile,
								'portBaseAuth' => $portBaseAuth ,
								'portBaseParameter' => $portBaseParameter ,
								'portBaseValue' => $portBaseValue ,
								'CGNAT' => $cgnat,
								'extra' => $extra,
								'desc' => "$desc",
								'update_date' => "$setup_date"
								);			
			
			return $results;			
		
	}


	public function radiusProfile() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$api_id			= $request->get('id', 'string');		
		$profile 		= $request->get('profile', 'string');
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);

				/*
                $rad_prof = $NewDBOBJ->query("SELECT * FROM radprofile");

                        while ($rad_prof_data   = $NewDBOBJ->fetch_row($rad_prof)) {
                                $radprof[]     	=  $rad_prof_data['profile'];
                        }
							$number_of_prof		= count($rad_prof);
							
				if( $number_of_prof < 1 ) {		

						throw new HTTPException(
												'Unable to find any profile',
												417,
												array(
														'dev' => '417',
														'internalCode' => '',
														'more' => 'Profile does not exist'
														//applicationCode => ''
													)
												);					
				}			
				*/
				
                $rad_user_profile = $NewDBOBJ->query("SELECT * FROM radgroupreply order by groupname");

                        while ($rad_user_profile_data     = $NewDBOBJ->fetch_row($rad_user_profile)) {
                                $radprofdata[$rad_user_profile_data['groupname']]['attribute'][] =  $rad_user_profile_data['attribute'];
								$radprofdata[$rad_user_profile_data['groupname']]['value'][] =  $rad_user_profile_data['value'];	
                        		
						}
								

						$results[] = array(
								'profiles' => $radprofdata
								);			
			
			return $results;			
		
	}

	public function detailSubscriber() {
		
		//date_default_timezone_set('UTC');
		
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$api_id			= $request->get('id', 'string');
		$username 		= $request->get('username', 'string');
		$precision 		= 2; //data calculation precision
		$data_unit 		= array("B", "KB", "MB", "GB");
	        $data_used		= "0";
		$rad_reply		= "";
		$radport		= "";
		$raduser		= "";	
		$bytes			= 0;	        
 
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date		= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date		= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);


                $rad_user = $NewDBOBJ->query("SELECT * FROM radcheck WHERE username='$username'");

                        while ($rad_user_data   = $NewDBOBJ->fetch_row($rad_user)) {
                                $raduser     	= $rad_user_data['username'];
								$proxy			= $rad_user_data['realm'];
                        }

                $rad_user_port = $NewDBOBJ->query("SELECT * FROM radportuser WHERE username='$username'");

                        while ($rad_user_port_data   = $NewDBOBJ->fetch_row($rad_user_port)) {
                                $radportauth     		 =  ( $rad_user_port_data['username']=='' ? 'NO' : 'YES' ) ;
								$raduserportparam     	 =  $rad_user_port_data['port'];
								$raduserportval     	 =  $rad_user_port_data['port_value'];
                        }
						
						if( !empty($radportauth) ) {
								$radportauth     		 = 'YES';	
								$radport				 = " OR callingstationid in ('$raduserportval','CHORUS$raduserportval','UFFNOV$raduserportval','ENNOV$raduserportval')";				 	
						} else {
								$radportauth     		 = 'NO';		
						}


				//radius auth logs
                $rad_auth_detail = $NewDBOBJ->query("SELECT * FROM radpostauth WHERE (username='$username' $radport) ORDER BY id DESC limit 500");

                        while ($rad_auth_data   	= $NewDBOBJ->fetch_row($rad_auth_detail)) {
                                $rad_auth_log[$rad_auth_data['id']]['password'] 	=  $rad_auth_data['pass'];
                                $rad_auth_log[$rad_auth_data['id']]['reply'] 	  	=  $rad_auth_data['reply'];
                                $rad_auth_log[$rad_auth_data['id']]['nasipaddress'] =  $rad_auth_data['nasipaddress'];
								$rad_auth_log[$rad_auth_data['id']]['nasportid'] 	=  $rad_auth_data['nasportid'];
								$rad_auth_log[$rad_auth_data['id']]['callingstationid'] =  $rad_auth_data['callingstationid'];
								$rad_auth_log[$rad_auth_data['id']]['authdate'] 		=  $rad_auth_data['authdate'];
                                								
                        }
				//radreply unique attributes per subscriber		
                $rad_reply_detail = $NewDBOBJ->query("SELECT * FROM radreply WHERE (username='$username')");

                        while ($rad_reply_data   	= $NewDBOBJ->fetch_row($rad_reply_detail)) {
                                $rad_reply[$rad_reply_data['attribute']][] 	=  $rad_reply_data['value'];                              								
                        }

				
				//get the last record.
                $rad_session_detail = $NewDBOBJ->query("SELECT * FROM radacct WHERE (username='$username' $radport) and framedipaddress!='' and acctoutputoctets>0  ORDER BY radacctid DESC limit 1");

                        while ($rad_session_data   	= $NewDBOBJ->fetch_row($rad_session_detail)) {
                                $radacctid     		=  $rad_session_data['radacctid'];
								$acctsessionid     	=  $rad_session_data['acctsessionid'];
								$acctuniqueid     	=  $rad_session_data['acctuniqueid'];
								$profile	     	=  $rad_session_data['profile'];
								$nasportid     		=  $rad_session_data['nasportid'];
								$nasporttype     	=  $rad_session_data['nasporttype'];
								$raduser     		=  $rad_session_data['username'];
								$acctstarttime     	=  $rad_session_data['acctstarttime'];
								$acctstoptime     	=  $rad_session_data['acctstoptime'];
								
								$actualdataratedown     =  $rad_session_data['actualdataratedown'];
								$actualdatarateup     	=  $rad_session_data['actualdatarateup'];
								
								$rad_acct_status    =  ( $rad_session_data['acctstoptime'] == '' ? 'Online' : 'Offline' ) ;
								$acctupdatetime     =  $rad_session_data['acctupdatetime'];
								$ip_address    		=  $rad_session_data['framedipaddress'];
								$ip_address_range   = preg_split("/[\s.]+/", "$ip_address");
								$cgnat				= ($ip_address_range[0]=="100" ? 'YES' : 'NO'); 
								$ipv6_address		=  $rad_session_data['frameddelegatedipv6address'];
								$framedprotocol     =  $rad_session_data['framedprotocol'];
								$bytes				= $rad_session_data['acctinputoctets'] +  $rad_session_data['acctoutputoctets'];
                        }

						
						$data_exp = floor(log($bytes, 1024)) | 0;
						$data_used = round($bytes / (pow(1024, $data_exp)), $precision).$data_unit[$data_exp];

				if( empty($raduser) ) {		

						throw new HTTPException(
												'Unable to find the subscriber',
												420,
												array(
														'dev' => '420',
														'internalCode' => '',
														'more' => 'Subscriber does not exist'
														//applicationCode => ''
													)
												);					
				}
			
			
			
				else {
					$results[] = array(
							'username' => $username,
							'profile' => $profile,
							'proxy' => $proxy,
							'portBaseAuth' => $radportauth,
							'portBaseParameter' => $raduserportparam,
							'portBaseValue' => $raduserportval,
							'radiusSessionID' => $acctsessionid,
							'radiusSessionStart' => $acctstarttime,
							'radiusSessionStop' => $acctstoptime,
							'data_used'				=> $data_used,
							'datarate_upstream'	=> $actualdatarateup,
							'datarate_downstream' => $actualdataratedown,
							'radiusSessionStatus' => $rad_acct_status,
							'radiusAuthLog' => $rad_auth_log,
							'framedProtocol' => $framedprotocol,
							'clientDNSPri' => $dns_pri,
							'clientDNSSec' => $dns_sec,
							'CGNAT' => $cgnat,
							'extra' => $rad_reply,
							'ip_address' => $ip_address,
							'ipv6_address' => $ipv6_address,
							'last_update_date' => $acctupdatetime
							);
				}
			
			
			return $results;					
		
	}

	public function liststaticIP() {
		
		//date_default_timezone_set('UTC');
		
		
		$validationObj 	= new \Feenix\Classes\Validation();
		$request 		= $this->di->get('request');
		$api_id			= $request->get('id', 'string');
        $ips			= "0";
		        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date		= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date		= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);




				//radius auth logs
                $rad_auth_detail = $NewDBOBJ->query("SELECT * FROM radpostauth WHERE username='$username' ORDER BY id DESC limit 100");

                        while ($rad_auth_data   	= $NewDBOBJ->fetch_row($rad_auth_detail)) {
                                $rad_auth_log[$rad_auth_data['id']]['password'] 	=  $rad_auth_data['pass'];
                                $rad_auth_log[$rad_auth_data['id']]['reply'] 	  	=  $rad_auth_data['reply'];
                                $rad_auth_log[$rad_auth_data['id']]['nasipaddress'] =  $rad_auth_data['nasipaddress'];
								$rad_auth_log[$rad_auth_data['id']]['nasportid'] 	=  $rad_auth_data['nasportid'];
								$rad_auth_log[$rad_auth_data['id']]['callingstationid'] =  $rad_auth_data['callingstationid'];
								$rad_auth_log[$rad_auth_data['id']]['authdate'] 		=  $rad_auth_data['authdate'];
                                								
                        }
				
				//get the last record.
                $rad_session_detail = $NewDBOBJ->query("SELECT * FROM radacct WHERE username='$username' and framedipaddress!='' and acctoutputoctets>0  ORDER BY radacctid DESC limit 1");

                        while ($rad_session_data   	= $NewDBOBJ->fetch_row($rad_session_detail)) {
                                $radacctid     		=  $rad_session_data['radacctid'];
								$acctsessionid     	=  $rad_session_data['acctsessionid'];
								$acctuniqueid     	=  $rad_session_data['acctuniqueid'];
								$profile	     	=  $rad_session_data['groupname'];
								$nasportid     		=  $rad_session_data['nasportid'];
								$nasporttype     	=  $rad_session_data['nasporttype'];
								$raduser     		=  $rad_session_data['username'];
								$acctstarttime     	=  $rad_session_data['acctstarttime'];
								$acctstoptime     	=  $rad_session_data['acctstoptime'];
								
								$actualdataratedown     =  $rad_session_data['actualdataratedown'];
								$actualdatarateup     	=  $rad_session_data['actualdatarateup'];
								
								$rad_acct_status    =  ( $rad_session_data['acctstoptime'] == '' ? 'Online' : 'Offline' ) ;
								$acctupdatetime     =  $rad_session_data['acctupdatetime'];
								$ip_address    		=  $rad_session_data['framedipaddress'];
								$ip_address_range   = preg_split("/[\s.]+/", "$ip_address");
								$cgnat				= ($ip_address_range[0]=="100" ? 'YES' : 'NO'); 
								$framedprotocol     =  $rad_session_data['framedprotocol'];
								$bytes				= $rad_session_data['acctinputoctets'] +  $rad_session_data['acctoutputoctets'];
                        }

						
						$data_exp = floor(log($bytes, 1024)) | 0;
						$data_used = round($bytes / (pow(1024, $data_exp)), $precision).$data_unit[$data_exp];

				if( empty($raduser) ) {		

						throw new HTTPException(
												'Unable to find the subscriber',
												420,
												array(
														'dev' => '420',
														'internalCode' => '',
														'more' => 'Subscriber does not exist'
														//applicationCode => ''
													)
												);					
				}
			
			
			
				else {
					$results[] = array(
							'username' => $username,
							'profile' => $profile,
							'portBaseAuth' => $radportauth,
							'portBaseParameter' => $raduserportparam,
							'portBaseValue' => $raduserportval,
							'radiusSessionID' => $acctsessionid,
							'radiusSessionStart' => $acctstarttime,
							'radiusSessionStop' => $acctstoptime,
							'data_used'				=> $data_used,
							'datarate_upstream'	=> $actualdatarateup,
							'datarate_downstream' => $actualdataratedown,
							'radiusSessionStatus' => $rad_acct_status,
							'radiusAuthLog' => $rad_auth_log,
							'framedProtocol' => $framedprotocol,
							'clientDNSPri' => $dns_pri,
							'clientDNSSec' => $dns_sec,
							'CGNAT' => $cgnat,
							'ip_address' => $ip_address,
							'last_update_date' => $acctupdatetime
							);
				}
			
			
			return $results;					
		
	}
		
	public function usageSubscriber() {
		
		date_default_timezone_set('UTC');
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$api_id			= $request->get('id', 'string');
		$username 		= $request->get('username', 'string');
		$startDate 		= $request->get('startDate', 'string');
		$endDate 		= $request->get('endDate', 'string');
		$precision 		= 2; //data calculation precision
		$data_unit 		= array("B", "KB", "MB", "GB");
        	$data_used		= "0";
		$limit			= "2020";
		$filtertime 	= "";
		        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
				if( preg_match("/-test$/",$api_id) ) {
					$rad_db_user    = $this->di->getShared('config')->radius_test->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius_test->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius_test->db_host;
					$rad_db     	= $this->di->getShared('config')->radius_test->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}
				else {
					$rad_db_user    = $this->di->getShared('config')->radius->db_username;
					$rad_db_pass    = $this->di->getShared('config')->radius->db_password;
					$rad_db_host    = $this->di->getShared('config')->radius->db_host;
					$rad_db     	= $this->di->getShared('config')->radius->dbname;
					$setup_date	= date($this->di->getShared('config')->radius->dformat);
				}

                $NewDBOBJ->sql_connect($rad_db_host,$rad_db_user,$rad_db_pass, $rad_db);


                $rad_user = $NewDBOBJ->query("SELECT * FROM radcheck WHERE username='$username'");

                        while ($rad_user_data   = $NewDBOBJ->fetch_row($rad_user)) {
                                $raduser     	=  $rad_user_data['username'];
                        }

				if( empty($raduser) ) {		

						throw new HTTPException(
												'Unable to find the subscriber',
												420,
												array(
														'dev' => '420',
														'internalCode' => '',
														'more' => 'Subscriber does not exist'
														//applicationCode => ''
													)
												);					
				}
			
				if( !empty($startDate) && empty($endDate) ) {

						throw new HTTPException(
												'Unable to recognize the date range',
												422,
												array(
														'dev' => '422',
														'internalCode' => '',
														'more' => 'Date range is not valid'
														//applicationCode => ''
													)
												);							
				}
				
				if( $startDate > $endDate ) {
					
						throw new HTTPException(
												'Unable to recognize the date range',
												422,
												array(
														'dev' => '422',
														'internalCode' => '',
														'more' => 'Date range is not valid'
														//applicationCode => ''
													)
												);
				}

				if( !empty($startDate) && !empty($endDate) ) {
						//$filtertime = " AND radacct_log.`timestamp` between '$startDate' and '$endDate'";
						$start = strtotime($startDate);
						$end   = strtotime($endDate);
						$limit = round(round(abs($end - $start) / 60)/10*2);
				}
				
				//get the data records.
				$rad_session_query	= "select * from radacct_log WHERE username='$username' order by id desc LIMIT $limit";
                
			$rad_session_detail = $NewDBOBJ->query($rad_session_query);

                        while ($rad_session_data   	= $NewDBOBJ->fetch_row($rad_session_detail)) {
                                
								$radacctid     		=  $rad_session_data['radacct_id'];
								$acctsessionid     	=  $rad_session_data['acctsessionid'];
								$raduser     		=  $rad_session_data['username'];
								$timestamp     		=  $rad_session_data['timestamp'];
								$datain				= $rad_session_data['acctinputoctets'];
								$dataout			= $rad_session_data['acctoutputoctets'];
								$bytes				= $datain +  $dataout;
								$data_exp 			= floor(log($bytes, 1024)) | 0;;
								$data_used 			= round($bytes / (pow(1024, $data_exp)), $precision).$data_unit[$data_exp];
								
								$results[] = array(
										'username' => $username,
										'radiusID' => $radacctid,
										'radiusSessionID' => $acctsessionid,
										'radiusAcctinputoctets' => $datain,
										'radiusAcctoutputoctets' => $dataout,
										'totalDataUsed'				=> $data_used,
										'timestamp' => $timestamp
										);								                       			
					    }

					
			return $results;					
		
	}

	public function acsDeviceTemplates() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$template 		= ( $request->get('template', 'string')!=' and acs_device_template_id=$template' ? '' : $request->get('template', 'string') );
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$update_date	= date($this->di->getShared('config')->acs->dformat);

                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);


                $acs_template = $NewDBOBJ->query("SELECT
acs_device_templates.acs_device_template_id,
acs_device_templates.acs_device_id,
acs_device_templates.acs_template_name,
acs_device_templates.acs_template_display_name,
acs_device_templates.active,
devices.device_id,
devices.device_name,
devices.active
FROM
devices
Inner Join acs_device_templates ON acs_device_templates.acs_device_id = devices.device_id
WHERE devices.active='Y' $template");

                        while ($acs_template_data   = $NewDBOBJ->fetch_row($acs_template)) {
                                $acstemplate[]     	=  $acs_template_data['acs_template_name'];
								$acs[$acs_template_data['acs_template_name']]['device_template_id']   =  $acs_template_data['acs_device_template_id'];
								$acs[$acs_template_data['acs_template_name']]['device_name']   =  $acs_template_data['device_name'];
                        }
							$number_of_templates	= count($acstemplate);
							
				if( $number_of_templates < 1 ) {		

						throw new HTTPException(
												'Unable to find any template',
												437,
												array(
														'dev' => '437',
														'internalCode' => '',
														'more' => 'ACS config template does not exist'
														//applicationCode => ''
													)
												);					
				}			
								

						$results[] = array(
								'number_of_acs_templates' => $number_of_templates,
								$acs
								);			
			
			return $results;			
		
	}

	public function acsDeviceList() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$update_date	= date($this->di->getShared('config')->acs->dformat);

                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);


                $acs_template = $NewDBOBJ->query("select acs.device_id , acs.serial_no , acs.status,acs_device_templates.acs_device_template_id,acs_device_templates.acs_template_display_name  from acs Inner Join acs_device_templates ON acs_device_templates.acs_device_id = acs.acs_template_id WHERE acs.active='Y'");

                        while ($acs_template_data   = $NewDBOBJ->fetch_row($acs_template)) {
                                $acs[$acs_template_data['device_id']]['serial_no']   =  $acs_template_data['serial_no'];
								$acs[$acs_template_data['device_id']]['device_template_id']   =  $acs_template_data['acs_device_template_id'];
								$acs[$acs_template_data['device_id']]['acs_template_name']   =  $acs_template_data['acs_template_display_name'];
								$acs[$acs_template_data['device_id']]['device_status']   =  $acs_template_data['status'];
								
                        }
								
								

						$results[] = array(
								'devices' => $acs
								);			
			
			return $results;			
		
	}
	
	public function acsCreateAccount() {
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$serial 		= ( $request->get('serial', 'string') );
		$template		= ( $request->get('template', 'string') );
		$company		= 1; //this is for nova
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$setup_date		= date($this->di->getShared('config')->acs->dformat);

                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);

				if( strlen($serial)<8 || strlen($serial)>20 ) {		

						throw new HTTPException(
												'Unable to setup the device',
												436,
												array(
														'dev' => '436',
														'internalCode' => '',
														'more' => 'ACS not accepting the serial number of the device'
														//applicationCode => ''
													)
												);					
				}	

				if( empty($template) ) {		

						throw new HTTPException(
												'Unable to setup the device template',
												435,
												array(
														'dev' => '435',
														'internalCode' => '',
														'more' => 'ACS not accepting the template'
														//applicationCode => ''
													)
												);					
				}	

                $acs_template = $NewDBOBJ->query("SELECT * FROM acs_device_templates WHERE acs_device_template_id='$template' and active='Y'");

                        while ($acs_template_data   = $NewDBOBJ->fetch_row($acs_template)) {
                                $acstemplate[]     	=  $acs_template_data['acs_template_name'];
                        }
							$number_of_templates	= count($acstemplate);

				if( $number_of_templates < 1  ) {		

						throw new HTTPException(
												'Unable to find any template',
												437,
												array(
														'dev' => '437',
														'internalCode' => '',
														'more' => 'ACS config template does not exist'
														//applicationCode => ''
													)
												);					
				}		



                $acs_serial = $NewDBOBJ->query("SELECT * FROM acs WHERE serial_no='$serial' and active='Y'");

                        while ($acs_serial_data   = $NewDBOBJ->fetch_row($acs_serial)) {
                                $acsserial[]    =  $acs_serial_data['serial_no'];
								$device_id	    =  $acs_serial_data['device_id'];
                        }
							$number_of_serial	= count($acsserial);

				if( $number_of_serial > 0 ) {		

									//ignore and dont insert again as its already there	
				}

				else {
					$NewDBOBJ->query("INSERT INTO acs(serial_no,customer_id,acs_template_id) values('$serial','$company','$template')");
					$device_id	= $NewDBOBJ->sql_id();	
				}
				
				$NewDBOBJ->query("INSERT IGNORE INTO acs_cache_logs(serial) values('$serial')");
				$NewDBOBJ->query("INSERT IGNORE INTO acs_event_queue(serial) values('$serial')");
				
				
				if( $device_id ) {
							$results[] = array(
								'device_id' => $device_id,
								'serial' => $serial,
								'template' => $template,
								'msg' => 'Your device has been setup',
								'setup_date' => $setup_date
								);			
								
				}
				
				else {
						throw new HTTPException(
												'Unable to setup the device',
												434,
												array(
														'dev' => '434',
														'internalCode' => '',
														'more' => 'ACS is unable to setup this device at this time'
														//applicationCode => ''
													)
												);					
					
				}

			return $results;								
		
	}

	public function acsDeviceUpdate() {
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$serial 		= ( $request->get('serial', 'string') );
		$template		= ( $request->get('template', 'string') );
		$company		= 1; //this is for nova
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$setup_date		= date($this->di->getShared('config')->acs->dformat);

                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);

				if( strlen($serial)<8 || strlen($serial)>20 ) {		

						throw new HTTPException(
												'Unable to setup the device',
												436,
												array(
														'dev' => '436',
														'internalCode' => '',
														'more' => 'ACS is not accepting the serial number of the device'
														//applicationCode => ''
													)
												);					
				}	

				if( empty($template) ) {		

						throw new HTTPException(
												'Unable to find the device template',
												435,
												array(
														'dev' => '435',
														'internalCode' => '',
														'more' => 'ACS is not accepting the template'
														//applicationCode => ''
													)
												);					
				}	

                $acs_template = $NewDBOBJ->query("SELECT * FROM acs_device_templates WHERE acs_device_template_id='$template' and active='Y'");

                        while ($acs_template_data   = $NewDBOBJ->fetch_row($acs_template)) {
                                $acstemplate[]     	=  $acs_template_data['acs_template_name'];
                        }
							$number_of_templates	= count($acstemplate);

				if( $number_of_templates < 1  ) {		

						throw new HTTPException(
												'Unable to find any template',
												437,
												array(
														'dev' => '437',
														'internalCode' => '',
														'more' => 'ACS config template does not exist'
														//applicationCode => ''
													)
												);					
				}		


                $acs_serial = $NewDBOBJ->query("SELECT * FROM acs WHERE serial_no='$serial' and active='Y'");

                        while ($acs_serial_data   = $NewDBOBJ->fetch_row($acs_serial)) {
                                $acsserial[]    =  $acs_serial_data['serial_no'];
                        }
							$number_of_serial	= count($acsserial);

				if( $number_of_serial > 0 ) {		

						throw new HTTPException(
												'Unable to setup the device',
												436,
												array(
														'dev' => '436',
														'internalCode' => '',
														'more' => 'ACS already have this serial'
														//applicationCode => ''
													)
												);					
				}


				$NewDBOBJ->query("INSERT INTO acs(serial_no,customer_id,acs_template_id) values('$serial','$company','$template')");
				$device_id	= $NewDBOBJ->sql_id();	
				$NewDBOBJ->query("INSERT INTO acs_cache_logs(serial) values('$serial')");
				$NewDBOBJ->query("INSERT INTO acs_event_queue(serial) values('$serial')");
				//$NewDBOBJ->query("INSERT INTO acs_events(serial) values('$serial')");
				
				
				if( $device_id ) {
							$results[] = array(
								'device_id' => $device_id,
								'serial' => $serial,
								'template' => $template,
								'msg' => 'Your device has been setup',
								'setup_date' => $setup_date
								);			
								
				}
				
				else {
						throw new HTTPException(
												'Unable to setup the device',
												434,
												array(
														'dev' => '434',
														'internalCode' => '',
														'more' => 'ACS is unable to setup this device at this time'
														//applicationCode => ''
													)
												);					
					
				}

			return $results;								
		
	}

        public function acsDeviceGetParameter() {
                //date_default_timezone_set('UTC');

                $validationObj  = new \Feenix\Classes\Validation();

                $request                = $this->di->get('request');
                $serial                 = ( $request->get('serial', 'string') );
                $getparam               = $request->get('getparameter');
                $getparam               = json_decode($getparam, true);

                $NewDBOBJ       = new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db         = $this->di->getShared('config')->acs->dbname;
                $setup_date             = date($this->di->getShared('config')->acs->dformat);



                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);

                                if( strlen($serial)<8 || strlen($serial)>20 ) {

                                                throw new HTTPException(
                                                                                                'Unable to setup the device',
                                                                                                436,
                                                                                                array(
                                                                                                                'dev' => '436',
                                                                                                                'internalCode' => '',
                                                                                                                'more' => 'ACS not accepting the serial number of the device'
                                                                                                                //applicationCode => ''
                                                                                                        )
                                                                                                );
                                }


                                        $no_of_getparam =count($getparam);
                                        if( $no_of_getparam > 0 ) {
                                                $NewDBOBJ->query("delete from acs_device_get_parameters where serial='$serial'");
                                                   foreach ($getparam as $p => $pval) {
                                                                        $pval_data = explode(",",$pval);
                                                                        if(count($pval_data) == 1) {
                                                                                $NewDBOBJ->query("INSERT INTO acs_device_get_parameters(serial,`parameter`,`request_date`,`paramter_value`) values('$serial','$p','$setup_date','$pval')");
                                                                        } else {
                                                                                for($pmval=0;$pmval<count($pval_data);$pmval++) {
                                                                                        $NewDBOBJ->query("INSERT INTO acs_device_get_parameters(serial,`parameter`,`request_date`,`paramter_value`) values('$serial','$p','$setup_date','".$pval_data[$pmval]."')");
                                                                                }
                                                                                unset($pval_data);//unset now
                                                                        }
                                                   }
                                        }

                                                        $results[] = array(
                                                                'serial' => $serial,
                                                                'getparameter' => $getparam,
                                                                'msg' => 'ACS will get the paramters shortly',
                                                                'setup_date' => $setup_date
                                                                );

                        return $results;

        }

	public function acsDeviceConfigPush() {
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 				= $this->di->get('request');
		$serial 				= ( $request->get('serial', 'string') );
		$template_push_type		= $validationObj->validateACSPushType($request->get('template_push_type', 'string')); //DEFAULT or CUSTOM
		$SSID_name_5GHZ			= ( $request->get('SSID_name_5GHZ', 'string') ); 
		$SSID_name_2GHZ			= ( $request->get('SSID_name_2GHZ', 'string') );
		$SSID_password_5GHZ			= ( $request->get('SSID_password_5GHZ', 'string') ); 
		$SSID_password_2GHZ			= ( $request->get('SSID_password_2GHZ', 'string') );
		//$connection_type			= $validationObj->validateACSDeviceConnectionType( $request->get('connection_type', 'string') );
		$connection_type 			='FIBRE'; //lagacy code
		$ntp_server					= ( $request->get('ntp_server', 'string') );
		//SIP line 1
		$SIP_registrar_line1		= ( $request->get('SIP_registrar_line1', 'string') );
		$SIP_outbound_proxy_line1	= ( $request->get('SIP_outbound_proxy_line1', 'string') );
		$SIP_proxy_server_line1		= ( $request->get('SIP_proxy_server_line1', 'string') );
		$SIP_proxy_server_transport_line1		= ( $request->get('SIP_proxy_server_transport_line1', 'string') );
		$SIP_auth_username_line1	= ( $request->get('SIP_auth_username_line1', 'string') );
		$SIP_auth_password_line1	= ( $request->get('SIP_auth_password_line1', 'string') );
		$SIP_cid_number_line1		= ( $request->get('SIP_cid_number_line1', 'string') );
		$SIP_cid_name_line1			= ( $request->get('SIP_cid_name_line1', 'string') );
		$SIP_session_expire_timer_line1			= ( $request->get('SIP_session_expire_timer_line1', 'string') );

		//SIP line 2
		$SIP_registrar_line2		= ( $request->get('SIP_registrar_line2', 'string') );
		$SIP_outbound_proxy_line2	= ( $request->get('SIP_outbound_proxy_line2', 'string') );
		$SIP_proxy_server_line2		= ( $request->get('SIP_proxy_server_line2', 'string') );
		$SIP_proxy_server_transport_line2		= ( $request->get('SIP_proxy_server_transport_line2', 'string') );
		$SIP_auth_username_line2	= ( $request->get('SIP_auth_username_line2', 'string') );
		$SIP_auth_password_line2	= ( $request->get('SIP_auth_password_line2', 'string') );
		$SIP_cid_number_line2		= ( $request->get('SIP_cid_number_line2', 'string') );
		$SIP_cid_name_line2			= ( $request->get('SIP_cid_name_line2', 'string') );
		$SIP_session_expire_timer_line2			= ( $request->get('SIP_session_expire_timer_line2', 'string') );
		
		$SIP_alg					= ( $request->get('SIP_alg', 'string') );
		$PPPOE_username				= ( $request->get('PPPOE_username', 'string') );
		$PPPOE_password				= ( $request->get('PPPOE_password', 'string') );
		$modem_password				= ( $request->get('modem_password', 'string') );
		$notify_url					= ( $request->get('notify_url', 'string') );
		$extra						= $request->get('extra');
		$extra						= json_decode($extra, true);		
		$event_type					= 'CONNECTION-REQUEST';
		
		$company		= 1; //this is for nova
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$setup_date		= date($this->di->getShared('config')->acs->dformat);

                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);

				if( strlen($serial)<8 || strlen($serial)>20 ) {		

						throw new HTTPException(
												'Unable to setup the device',
												436,
												array(
														'dev' => '436',
														'internalCode' => '',
														'more' => 'ACS is not accepting the serial number of the device'
														//applicationCode => ''
													)
												);					
				}	





                $acs_serial = $NewDBOBJ->query("SELECT * FROM acs WHERE serial_no='$serial' and active='Y'");

                        while ($acs_serial_data   = $NewDBOBJ->fetch_row($acs_serial)) {
                                $is_already_there    =  $acs_serial_data['serial_no'];
								$device_id			 =  $acs_serial_data['device_id'];
                        }

				if( empty($is_already_there) ) {		

						throw new HTTPException(
												'Unable to find the serial number',
												439,
												array(
														'dev' => '439',
														'internalCode' => '',
														'more' => 'ACS does not have any record of this serial number'
														//applicationCode => ''
													)
												);					
				}

				if( $template_push_type=='DEFAULT' ) {
					$NewDBOBJ->query("INSERT INTO acs_device_config_push(serial,request_date) values('$serial','$setup_date')");
					$device_push_id	= $NewDBOBJ->sql_id();	
					$NewDBOBJ->query("INSERT INTO acs_events(serial,request_date,create_template,notify_url,event) values('$serial','$setup_date','Y','$notify_url','$event_type')");
				}
				
				else {
					$NewDBOBJ->query("INSERT INTO acs_device_config_push(serial,template_update_type,modem_admin_password,ssid_name_5,ssid_name_2,ssid_pass_5,ssid_pass_2,sip_registrar_line1,sip_outbound_proxy_line1,sip_proxy_server_line1,sip_auth_username_line1,sip_auth_password_line1,sip_cid_number_line1,sip_cid_name_line1,pppoe_username,pppoe_password,request_date,connection_type,sip_proxy_server_session_expire_timer_line1,sip_alg,sip_proxy_server_transport_line1) values('$serial','$template_push_type','$modem_password','$SSID_name_5GHZ','$SSID_name_2GHZ','$SSID_password_5GHZ','$SSID_password_2GHZ','$SIP_registrar_line1','$SIP_outbound_proxy_line1','$SIP_proxy_server_line1','$SIP_auth_username_line1','$SIP_auth_password_line1','$SIP_cid_number_line1','$SIP_cid_name_line1','$PPPOE_username','$PPPOE_password','$setup_date','$connection_type','$SIP_session_expire_timer_line1','$SIP_alg','$SIP_proxy_server_transport_line1')");
					$device_push_id	= $NewDBOBJ->sql_id();	
					$no_of_extra =count($extra); 
					
					if( $no_of_extra > 0 ) {
						$NewDBOBJ->query("delete from acs_device_config_push_extra where serial='$serial'");
						   foreach ($extra as $p => $pval) {
							   		$pval_data = explode(",",$pval);
									if(count($pval_data) == 1) {
										$NewDBOBJ->query("INSERT INTO acs_device_config_push_extra(serial,`attribute`,`op`,`value`) values('$serial','$p',':','$pval')");
									} else {
										for($pmval=0;$pmval<count($pval_data);$pmval++) {
											$NewDBOBJ->query("INSERT INTO acs_device_config_push_extra(serial,`attribute`,`op`,`value`) values('$serial','$p',':','".$pval_data[$pmval]."')");	
										}
										unset($pval_data);//unset now
									}
						   }	
					}
					
					$NewDBOBJ->query("INSERT INTO acs_events(serial,request_date,create_template,notify_url,event) values('$serial','$setup_date','Y','$notify_url','$event_type')");
				}
				
				if( $device_push_id ) {
							$results[] = array(
								'device_id' => $device_id,
								'serial' => $serial,
								'coonfig_push_id' => $device_push_id,
								'extra' => $extra,
								'config_push_status' => "Queued",
								'msg' => 'Your device config will be updated shortly',
								'notify_url' => $notify_url,
								'request_date' => $setup_date
								);			
								
				}
				
				else {
						throw new HTTPException(
												'Unable to update the config',
												438,
												array(
														'dev' => '438',
														'internalCode' => '',
														'more' => 'Config update is already in process, please wait'
														//applicationCode => ''
													)
												);					
					
				}

			return $results;								
		
	}


	public function acsStatus() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$serial 		= $request->get('serial', 'string') ;
		$permission_key		= "$serial-permission";
		$state_key		= "$serial-state";
		$inform_key		= "$serial-parameters";
		$acs_permission	= "";
		$acs_state		= "";
		$acs_inform		= "";
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$acs_cache_host = $this->di->getShared('config')->acs->acs_cache_host;
				$acs_cache_port = $this->di->getShared('config')->acs->acs_cache_port;
				$update_date	= date($this->di->getShared('config')->acs->dformat);

                $acs_inform_from_cache = \Feenix\Classes\Helper::redis()->get($inform_key);
                $acs_inform_from_cache = json_decode($acs_inform_from_cache);

                $acs_permission_from_cache = \Feenix\Classes\Helper::redis()->get($permission_key);
                $acs_permission_from_cache = json_decode($acs_permission_from_cache);
		
                $acs_state_from_cache = \Feenix\Classes\Helper::redis()->get($state_key);
                $acs_state_from_cache = json_decode($acs_state_from_cache);




						//$device_id = $acs_permission->DeviceID;
						$device_id  = $acs_permission_from_cache->DeviceID;
	
						//$customer  = $acs_permission->Customer;
						$customer  = $acs_permission_from_cache->Customer;

						//$device_status= $acs_permission->Status;
						$device_status= $acs_permission_from_cache->Status;

						//$oui			= $acs_state->DeviceId->OUI;
						//$product_class	= $acs_state->DeviceId->ProductClass;
						//$manufacturer		= $acs_state->DeviceId->Manufacturer;
						$oui                = $acs_state_from_cache->DeviceId->OUI;
                        $product_class      = $acs_state_from_cache->DeviceId->ProductClass;
                        $manufacturer       = $acs_state_from_cache->DeviceId->Manufacturer;
						
                                $current_date=time();
                                $updated_date= $acs_inform_from_cache->{updated}->{updated};
                                $last_updated= ($current_date-$updated_date);

                                                if( $last_updated < 130 ) {
                                                        $status                 = "CPE is Online";
                                                } else {
                                                        $oui                    = "Unknown";
                                                        $product_class  = "Unknown";
                                                        $manufacturer   = "Unknown";
                                                        $status                 = "Unreachable";
                                                }


							
				if( empty($device_id) ) {		

						throw new HTTPException(
												'Unable to find the device',
												442,
												array(
														'dev' => '442',
														'internalCode' => '',
														'more' => 'Device is not configured in the ACS yet'
														//applicationCode => ''
													)
												);					
				}			
								

						$results[] = array(
								'serial' => $serial,
								'device_id' => $device_id,
								'customer' => $customer,
								'device_id' => $device_id,
								'manufacturer' => $manufacturer,
								'product_class' => $product_class,
								'inform' => $acs_inform_from_cache,
								'oui' => $oui,
								'device_status' => $status
								);			
			
			return $results;			
		
	}


	public function acsBulkStatus() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$acs_cache_host = $this->di->getShared('config')->acs->acs_cache_host;
				$acs_cache_port = $this->di->getShared('config')->acs->acs_cache_port;
				$update_date	= date($this->di->getShared('config')->acs->dformat);
				
				

		$serials 		= explode(",",$request->get('serial', 'string'),100) ;
		$no_of_serial   = count($serials);

                if( empty($request->get('serial')) ) {

                        $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);


                        $acs_log = $NewDBOBJ->query("SELECT serial_no from acs where active='Y'");
                        while ($acs_log_data   = $NewDBOBJ->fetch_row($acs_log)) {

                                $serial = $acs_log_data['serial_no'];

                                $permission_key         = "$serial-permission";
                                $state_key                      = "$serial-state";
                                $inform_key                     = "$serial-parameters";


                $acs_inform_from_cache = \Feenix\Classes\Helper::redis()->get($inform_key);
                $acs_inform_from_cache = json_decode($acs_inform_from_cache);

                $acs_permission_from_cache = \Feenix\Classes\Helper::redis()->get($permission_key);
                $acs_permission_from_cache = json_decode($acs_permission_from_cache);

                $acs_state_from_cache = \Feenix\Classes\Helper::redis()->get($state_key);
                $acs_state_from_cache = json_decode($acs_state_from_cache);

                                                $device_id              = $acs_permission_from_cache->DeviceID;
                                if( !empty($device_id) ) {

                                                $customer                       = $acs_permission_from_cache->Customer;
                                                $device_status          = $acs_permission_from_cache->Status;
                                                $oui                = $acs_state_from_cache->DeviceId->OUI;
                        $product_class      = $acs_state_from_cache->DeviceId->ProductClass;
                        $manufacturer       = $acs_state_from_cache->DeviceId->Manufacturer;

                                $current_date=time();
                                $updated_date= $acs_inform_from_cache->{updated}->{updated};
                                $last_updated= ($current_date-$updated_date);

                                                if( $last_updated < 130 ) {
                                                        $status                 = "CPE is Online";
                                                } else {
                                                        $oui                    = "Unknown";
                                                        $product_class  = "Unknown";
                                                        $manufacturer   = "Unknown";
                                                        $status                 = "Unreachable";
                                                }

	
                                                $results[] = array(
                                                                'serial' => $serial,
                                                                'device_id' => $device_id,
                                                                'customer' => $customer,
                                                                'device_id' => $device_id,
                                                                'manufacturer' => $manufacturer,
                                                                'product_class' => $product_class,
                                                                'inform' => $acs_inform_from_cache,
                                                                'oui' => $oui,
                                                                'device_status' => $status
                                                                );
                                }


                        }




                }
	
		
		if($no_of_serial>0) {
			
			for($i=0;$i<$no_of_serial;$i++) {
				
				$serial 			= $serials[$i];
				$permission_key		= "$serial-permission";
				$state_key			= "$serial-state";
				$inform_key			= "$serial-parameters";
		

                $acs_inform_from_cache = \Feenix\Classes\Helper::redis()->get($inform_key);
                $acs_inform_from_cache = json_decode($acs_inform_from_cache);

                $acs_permission_from_cache = \Feenix\Classes\Helper::redis()->get($permission_key);
                $acs_permission_from_cache = json_decode($acs_permission_from_cache);
		
                $acs_state_from_cache = \Feenix\Classes\Helper::redis()->get($state_key);
                $acs_state_from_cache = json_decode($acs_state_from_cache);

						$device_id  		= $acs_permission_from_cache->DeviceID;
						$customer  			= $acs_permission_from_cache->Customer;
						$device_status		= $acs_permission_from_cache->Status;
						$oui                = $acs_state_from_cache->DeviceId->OUI;
                        $product_class      = $acs_state_from_cache->DeviceId->ProductClass;
                        $manufacturer       = $acs_state_from_cache->DeviceId->Manufacturer;
						
                                $current_date=time();
                                $updated_date= $acs_inform_from_cache->{updated}->{updated};
                                $last_updated= ($current_date-$updated_date);

                                                if( $last_updated < 130 ) {
                                                        $status                 = "CPE is Online";
                                                } else {
                                                        $oui                    = "Unknown";
                                                        $product_class  = "Unknown";
                                                        $manufacturer   = "Unknown";
                                                        $status                 = "Unreachable";
                                                }


			/*					
				if( empty($device_id) ) {		

						throw new HTTPException(
												'Unable to find the device',
												442,
												array(
														'dev' => '442',
														'internalCode' => '',
														'more' => 'Device is not configured in the ACS yet'
														//applicationCode => ''
													)
												);					
				}			
			*/					

						$results[] = array(
								'serial' => $serial,
								'device_id' => $device_id,
								'customer' => $customer,
								'device_id' => $device_id,
								'manufacturer' => $manufacturer,
								'product_class' => $product_class,
								'inform' => $acs_inform_from_cache,
								'oui' => $oui,
								'device_status' => $status
								);		
			}//loop for serials
		}//if $no_of_serial>0
		
			
			return $results;			
		
	}



	public function acsDeleteAccount() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$serial 		= $request->get('serial', 'string') ;
        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$acs_cache_host = $this->di->getShared('config')->acs->acs_cache_host;
				$acs_cache_port = $this->di->getShared('config')->acs->acs_cache_port;
				$update_date	= date($this->di->getShared('config')->acs->dformat);


                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);


                $acs = $NewDBOBJ->query("SELECT * FROM acs WHERE serial_no='$serial' and active='Y' ");

                        while ($acs_data   = $NewDBOBJ->fetch_row($acs)) {
								$device_id = $acs_data['device_id'];
                        }

							
				if( empty($device_id) ) {		

						throw new HTTPException(
												'Unable to find the device',
												442,
												array(
														'dev' => '442',
														'internalCode' => '',
														'more' => 'Device is not configured in the ACS yet'
														//applicationCode => ''
													)
												);					
				}			
						$NewDBOBJ->query("DELETE FROM acs WHERE serial_no='$serial' ");
						$NewDBOBJ->query("DELETE FROM acs_cache_logs WHERE serial='$serial' ");		
						$NewDBOBJ->query("DELETE FROM acs_event_queue WHERE serial='$serial' ");		

						$results[] = array(
								'serial' => $serial,
								'device_id' => $device_id,
								'msg' => "Device has been deleted from the ACS"
								);			
			
			return $results;			
		
	}
	
	public function acsDeviceReboot() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$serial 		= $request->get('serial', 'string') ;
		$notify_url		= $request->get('notify_url', 'string') ;

                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$acs_cache_host = $this->di->getShared('config')->acs->acs_cache_host;
				$acs_cache_port = $this->di->getShared('config')->acs->acs_cache_port;
				$update_date	= date($this->di->getShared('config')->acs->dformat);

                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);

                $acs_serial = $NewDBOBJ->query("SELECT * FROM acs WHERE serial_no='$serial' and active='Y'");

                        while ($acs_serial_data   = $NewDBOBJ->fetch_row($acs_serial)) {
                                $acsserial    =  $acs_serial_data['serial_no'];
                        }

				if( empty($acsserial) ) {		

						throw new HTTPException(
												'Unable to find the device',
												442,
												array(
														'dev' => '442',
														'internalCode' => '',
														'more' => 'Device is not configured in the ACS yet'
														//applicationCode => ''
													)
												);					
				}
				
                $acs_events = $NewDBOBJ->query("SELECT * FROM acs_events WHERE serial='$serial' and event='REBOOT' and enqueue='N'");

                        while ($acs_queue_data   = $NewDBOBJ->fetch_row($acs_events)) {
                                $acsenqueue    	=  $acs_queue_data['serial_no'];
                        }

				if( !empty($acsenqueue) ) {		

						throw new HTTPException(
												'Unable to perform the operation',
												443,
												array(
														'dev' => '443',
														'internalCode' => '',
														'more' => 'The operation is already in the queue'
														//applicationCode => ''
													)
												);					
				}
				
								
				$NewDBOBJ->query("INSERT INTO acs_events(serial,event,enqueue,notify_url,request_date) values('$serial','REBOOT','N','$notify_url','$update_date')");
				$event_id	= $NewDBOBJ->sql_id();
				$results[] = array(
								'serial' => $serial,
								'event_id' => $event_id
								);			
			
			return $results;

	}
	public function acsDeviceFactoryReset() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$serial 		= $request->get('serial', 'string') ;	
		$notify_url		= $request->get('notify_url', 'string') ;

               $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$acs_cache_host = $this->di->getShared('config')->acs->acs_cache_host;
				$acs_cache_port = $this->di->getShared('config')->acs->acs_cache_port;
				$update_date	= date($this->di->getShared('config')->acs->dformat);

                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);

                $acs_serial = $NewDBOBJ->query("SELECT * FROM acs WHERE serial_no='$serial' and active='Y'");

                        while ($acs_serial_data   = $NewDBOBJ->fetch_row($acs_serial)) {
                                $acsserial    =  $acs_serial_data['serial_no'];
                        }

				if( empty($acsserial) ) {		

						throw new HTTPException(
												'Unable to find the device',
												442,
												array(
														'dev' => '442',
														'internalCode' => '',
														'more' => 'Device is not configured in the ACS yet'
														//applicationCode => ''
													)
												);					
				}
				
                $acs_events = $NewDBOBJ->query("SELECT * FROM acs_events WHERE serial='$serial' and event='FACTORY-RESET' and enqueue='N'");
				
                        while ($acs_queue_data   = $NewDBOBJ->fetch_row($acs_events)) {
                                $acsenqueue    	=  $acs_queue_data['serial_no'];
                        }

				if( !empty($acsenqueue) ) {		

						throw new HTTPException(
												'Unable to perform the operation',
												443,
												array(
														'dev' => '443',
														'internalCode' => '',
														'more' => 'The operation is already in the queue'
														//applicationCode => ''
													)
												);					
				}
				
								
				$NewDBOBJ->query("INSERT INTO acs_events(serial,`event`,notify_url,request_date) values('$serial','FACTORY-RESET','$notify_url','$update_date')");	

				$event_id	= $NewDBOBJ->sql_id();
				$results[] = array(
								'serial' => $serial,
								'event_id' => $event_id
								);			
			
			return $results;

	}

	public function acsEventQueue() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$serial 		= $request->get('serial', 'string') ;
		$acs_queue		= '';	

        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$acs_cache_host = $this->di->getShared('config')->acs->acs_cache_host;
				$acs_cache_port = $this->di->getShared('config')->acs->acs_cache_port;
				$update_date	= date($this->di->getShared('config')->acs->dformat);


                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);

                $acs_serial = $NewDBOBJ->query("SELECT * FROM acs WHERE serial_no='$serial' and active='Y'");

                        while ($acs_serial_data   = $NewDBOBJ->fetch_row($acs_serial)) {
                                $acsserial    =  $acs_serial_data['serial_no'];
                        }

				if( empty($acsserial) ) {		

						throw new HTTPException(
												'Unable to find the device',
												442,
												array(
														'dev' => '442',
														'internalCode' => '',
														'more' => 'Device is not configured in the ACS yet'
														//applicationCode => ''
													)
												);					
				}

                $acs_queue_log = $NewDBOBJ->query("SELECT * FROM acs_event_queue WHERE serial='$serial' ");

                        while ($acs_log_data   = $NewDBOBJ->fetch_row($acs_queue_log)) {
								$acs_queue	 	= json_decode($acs_log_data['current_queue_data']);
                        }
						
						//$queue_data = $acs_queue->data;		


						$results[] = array(
								'serial' => $serial,
								'queue' => $acs_queue
								);			
			
			return $results;	


	}	
	

	public function acsEventLog() {
		
		//date_default_timezone_set('UTC');
		
		$validationObj 	= new \Feenix\Classes\Validation();
		
		$request 		= $this->di->get('request');
		$serial 		= $request->get('serial', 'string') ;
		$acs_log		= '';	

        
                $NewDBOBJ      	= new \Feenix\Classes\MySQLDB();
                $acs_db_user    = $this->di->getShared('config')->acs->db_username;
                $acs_db_pass    = $this->di->getShared('config')->acs->db_password;
                $acs_db_host    = $this->di->getShared('config')->acs->db_host;
                $acs_db     	= $this->di->getShared('config')->acs->dbname;
				$acs_cache_host = $this->di->getShared('config')->acs->acs_cache_host;
				$acs_cache_port = $this->di->getShared('config')->acs->acs_cache_port;
				$update_date	= date($this->di->getShared('config')->acs->dformat);


                $NewDBOBJ->sql_connect($acs_db_host,$acs_db_user,$acs_db_pass, $acs_db);

                $acs_serial = $NewDBOBJ->query("SELECT * FROM acs WHERE serial_no='$serial' and active='Y'");

                        while ($acs_serial_data   = $NewDBOBJ->fetch_row($acs_serial)) {
                                $acsserial    =  $acs_serial_data['serial_no'];
                        }

				if( empty($acsserial) ) {		

						throw new HTTPException(
												'Unable to find the device',
												442,
												array(
														'dev' => '442',
														'internalCode' => '',
														'more' => 'Device is not configured in the ACS yet'
														//applicationCode => ''
													)
												);					
				}

                $acs_event_log = $NewDBOBJ->query("SELECT * FROM acs_events WHERE serial='$serial' order by acs_event_id desc limit 10 ");

                        while ($acs_log_data   = $NewDBOBJ->fetch_row($acs_event_log)) {
								$acs_log[$acs_log_data['acs_event_id']]['event']	 		= $acs_log_data['event'];
								$acs_log[$acs_log_data['acs_event_id']]['response']	 		= $acs_log_data['result'];
								$acs_log[$acs_log_data['acs_event_id']]['request_date']	 	= $acs_log_data['request_date'];
								$acs_log[$acs_log_data['acs_event_id']]['response_date']	= $acs_log_data['enqueue_date'];
                        }
						
						//$queue_data = $acs_queue->data;		


						$results[] = array(
								'serial' => $serial,
								'event_log' => $acs_log
								);			
			
			return $results;	


	}	
			
	
}
