<?php
/*
Please put any business logic in the validation class.
*/

namespace Feenix\Classes;
use Feenix\Exceptions\HTTPException;

class Validation {
	


	public function validateMMEmail($email){
	
		if( strlen($email) < 1 ){
			throw new HTTPException(
						'Email field can not be empty',
						402,
						array(
							'dev' => '',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			throw new HTTPException(
						'Invalid Email Address Format',
						403,
						array(
							'dev' => '',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		}
		return $email;
	}


	public function validateMyUsername($username){
	
		if( strlen($username) != 7 || !filter_var($username, FILTER_VALIDATE_INT) ){
			throw new HTTPException(
						'Invalid Username or Password',
						401,
						array(
							'dev' => '401',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}
		
		return $username;
	}	
	
	public function validateMyPassword($password){
	
		if( strlen($password) < 6 ){
			throw new HTTPException(
						'Invalid Username or Password',
						401,
						array(
							'dev' => '401',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}
		
		return $password;
	}	


	public function validateMyEmail($email){
	
		if( strlen($email) < 1 ){
			throw new HTTPException(
						'Email field can not be empty',
						402,
						array(
							'dev' => '402',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}
		
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			throw new HTTPException(
						'Invalid Email Address Format',
						403,
						array(
							'dev' => '403',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		}
		return $email;
	}



	public function validateCallType($call_type){
	
		$call_type			= strtoupper($call_type);
		$valid_call_type 	= array("INTERNAL", "OUTBOUND", "MISSED", "INCOMING", "INBOUND", "ALL");
		
		if( !in_array($call_type,$valid_call_type) ){
			throw new HTTPException(
						'Invalid Call Type',
						409,
						array(
							'dev' => '409',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $call_type;
	}

	public function validateradclientEventType($event_type){

                $event_type              = strtoupper($event_type);
                $valid_event_type        = array("COA", "DISCONNECT");

                if( !in_array($event_type,$valid_event_type) ){
                        throw new HTTPException(
                                                'Invalid Event Type',
                                                412,
                                                array(
                                                        'dev' => '412',
                                                        'internalCode' => ''
                                                        //'more' => '',
                                                        //applicationCode => ''
                                                )
                        );

                }

                return strtolower($event_type);
        }

	public function validatebolean($input){
	
		$input			= strtoupper($input);
		$valid_input 	= array("YES", "NO");
		
		if( !in_array($input,$valid_input) ){
			throw new HTTPException(
						"Invalid $input",
						409,
						array(
							'dev' => '409',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $input;
	}

	public function validateRecordingFormat($input){
	
		$input			= strtolower($input);
		$valid_input 	= array("gsm", "wav", "mp3");
		
		if( !in_array($input,$valid_input) ){
			throw new HTTPException(
						"Invalid $input",
						411,
						array(
							'dev' => '411',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $input;
	}

	public function validateRouteProto($input){
	
		$input			= strtoupper($input);
		$valid_input 	= array("UDP", "TCP");
		
		if( !in_array($input,$valid_input) ){
			throw new HTTPException(
						"Invalid Proto $input",
						409,
						array(
							'dev' => '409',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $input;
	}

	public function validateSIPAuthType($input){
	
		$input			= strtoupper($input);
		$valid_input 	= array("SIP_REG_AUTH", "SIP_IP_AUTH");
		
		if( !in_array($input,$valid_input) ){
			throw new HTTPException(
						"Invalid SIP Auth type $input",
						409,
						array(
							'dev' => '409',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $input;
	}
		
	public function validateCallForwardRouteType($route_type){
	
		$input			= strtoupper($route_type);
		$valid_route_type 	= array("CHK_PHONE_STATUS", "ALWAYS_FORWARD");
		
		if( !in_array($route_type,$valid_route_type) ){
			throw new HTTPException(
						"Invalid $input",
						410,
						array(
							'dev' => '410',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $route_type;
	}	

	public function validateCalleridFormat($callerid){
	
		
		if( strlen($callerid) < 7 || !filter_var($username, FILTER_VALIDATE_INT) ){
			throw new HTTPException(
						"Invalid $input",
						410,
						array(
							'dev' => '410',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $callerid;
	}	

	public function validateDIDQuantity($quantity) {
		
		$valid_quantity 	= array("1", "10","100");
		
		if( !in_array($quantity,$valid_quantity) ){
			throw new HTTPException(
						"Invalid Quantity $input",
						415,
						array(
							'dev' => '415',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $quantity;		
	}

	public function validateDIDPeriod($period) {
				
		if(  $period >99 || $period < 1 ){
			throw new HTTPException(
						"Unable to recognize the period $period",
						416,
						array(
							'dev' => '416',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $period;		
	}

	public function validateIPAddress($ip){
			
		if(!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
			throw new HTTPException(
						"Invalid IP Address",
						413,
						array(
							'dev' => '113',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $ip;
	}	
	//validateACSDeviceConnectionType

	public function validateACSDeviceConnectionType($type){
	
		$input			= strtoupper($type);
		$valid_type 	= array("VDSL", "ADSL","FIBRE");
		
		if( !in_array($type,$valid_type) ){
			throw new HTTPException(
						"Invalid $input",
						410,
						array(
							'dev' => '410',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $type;
	}
	
	public function validateACSPushType($type){
	
		$input			= strtoupper($type);
		$valid_type 	= array("DEFAULT", "CUSTOM");
		
		if( !in_array($type,$valid_type) ){
			throw new HTTPException(
						"Invalid $input",
						410,
						array(
							'dev' => '410',
							'internalCode' => ''
							//'more' => '',
							//applicationCode => ''
						)
			);
		
		}

		return $type;
	}	


}
