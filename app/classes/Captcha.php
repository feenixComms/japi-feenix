<?php
namespace Feenix\Classes;
class Captcha {

	function hexrgb ($hexstr)
	{
	  $int = hexdec($hexstr);
	
	  return array("red" => 0xFF & ($int >> 0x10),
				   "green" => 0xFF & ($int >> 0x8),
				   "blue" => 0xFF & $int);
	}
	
	function create_captcha($key,$width,$height,$textcolour){
		
		
	//Settings for captcha
	$image_width = $width;
	$image_height = $height;
	$characters_on_image = 6;
	$font = 'images/fonts/monofont.ttf';
	$captcha_id 		= '';
	$captcha_id_length 	= 10;
	//The characters that can be used in the CAPTCHA code.
	//avoid confusing characters (l 1 and i for example)
	$possible_letters = '23456789bcdfghjkmnpqrstvwxyz';
	$random_dots = rand(10,20);
	$random_lines = rand(10,20);
	$captcha_text_color=$textcolour;
	$captcha_noice_color = "0x142864";
	$captcha_id			= rand(substr(time(),1,$captcha_id_length),99999999999);;
	$file_name 			= rand().'.jpg';
	$file_name_dest		='/tmp/'."$file_name";

	$code = '';
	$link = '';
	
	
	$i = 0;
	while ($i < $characters_on_image) { 
	$code .= substr($possible_letters, mt_rand(0, strlen($possible_letters)-1), 1);
	$i++;
	}
	
	
	$font_size = $image_height * 0.70;
	$image = @imagecreate($image_width, $image_height);
	
	
	/* setting the background, text and noise colours here */
	$background_color = imagecolorallocate($image, 255, 255, 255);
	
	$arr_text_color = CAPTCHA::hexrgb($captcha_text_color);
	$text_color = imagecolorallocate($image, $arr_text_color['red'], 
			$arr_text_color['green'], $arr_text_color['blue']);
	
	$arr_noice_color = CAPTCHA::hexrgb($captcha_noice_color);
	$image_noise_color = imagecolorallocate($image, $arr_noice_color['red'], 
			$arr_noice_color['green'], $arr_noice_color['blue']);
	
	
	/* generating the dots randomly in background */
	for( $i=0; $i<$random_dots; $i++ ) {
	imagefilledellipse($image, mt_rand(0,$image_width),
	 mt_rand(0,$image_height), 2, 3, $image_noise_color);
	}
	
	
	/* generating lines randomly in background of image */
	for( $i=0; $i<$random_lines; $i++ ) {
	imageline($image, mt_rand(0,$image_width), mt_rand(0,$image_height),
	 mt_rand(0,$image_width), mt_rand(0,$image_height), $image_noise_color);
	}
	
	
	/* create a text box and add code in it */
	$textbox = imagettfbbox($font_size, 0, $font, $code); 
	$x = ($image_width - $textbox[4])/2;
	$y = ($image_height - $textbox[5])/2;
	imagettftext($image, $font_size, 0, $x, $y, $text_color, $font , $code);
	
	
	/* Show captcha image in the page html page */
	//header('Content-Type: image/jpeg');// defining the image type to be shown in browser widow
	$fh=fopen($file_name_dest,'w');
	fclose($fh);
	imagejpeg($image,"$file_name_dest");//showing the image
	imagedestroy($image);//destroying the image instance
	
	$link = "https://api001.test.com/webservices/captcha/$file_name";
	return array("$code","$link","$captcha_id");
	}
}//class end
