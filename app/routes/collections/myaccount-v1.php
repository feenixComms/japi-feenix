<?php
/**
 * Collections let us define groups of routes that will all use the same controller.
 * We can also set the handler to be lazy loaded.  Collections can share a common prefix.
 * @var $accountCollection
 * This is an Immeidately Invoked Function in php.  Anonymous function will be returned to any file that "includes" it. 
 */


return call_user_func(function(){

	$accountCollection = new \Phalcon\Mvc\Micro\Collection();

	$accountCollection
		// VERSION NUMBER SHOULD BE FIRST URL PARAMETER, ALWAYS
		->setPrefix('/feenix/v1')
		// Must be a string in order to support lazy loading
		->setHandler('\Feenix\Controllers\FeenixVoiceController')
		->setLazy(true);

	/****** voice apis ********/
	
	$accountCollection->post('/login', 'login');   
	$accountCollection->post('/voice/login', 'login');
        $accountCollection->post('/voice/view', 'login');
        $accountCollection->post('/voice/sip/update', 'sipupdate');
	$accountCollection->post('/voice/phone/add', 'phoneadd');
        $accountCollection->post('/voice/phone/update', 'phoneupdate');
        $accountCollection->post('/voice/phone/available', 'phoneavailable');
	$accountCollection->post('/voice/accounts/setup', 'createaccount');
	$accountCollection->post('/voice/view/callrate', 'callrate');
	$accountCollection->post('/voice/view/timezone', 'voicetimezone');
	$accountCollection->post('/voice/view/calldetails', 'calldetails');
	
	/***** radius apis *******/

        $accountCollection->post('/subscriber/radclient', 'useradclient');	
	$accountCollection->post('/subscriber/accounts/setup', 'createradiusaccount');
	$accountCollection->post('/subscriber/accounts/list', 'listradiusaccount');
	$accountCollection->post('/subscriber/accounts/update', 'updateradiusaccount');
	$accountCollection->post('/subscriber/accounts/delete', 'deleteradiusaccount');
	$accountCollection->post('/subscriber/accounts/rename', 'renameradiusaccount');
	$accountCollection->post('/subscriber/accounts/detail', 'detailradiusaccount');
	$accountCollection->post('/subscriber/accounts/profile', 'radiusprofile');
	$accountCollection->post('/subscriber/accounts/profile/setup', 'createradiusprofile');
	$accountCollection->post('/subscriber/accounts/profile/update', 'updateradiusprofile');
	$accountCollection->post('/subscriber/accounts/profile/delete', 'deleteradiusprofile');
	$accountCollection->post('/subscriber/accounts/datausage', 'usageradiusaccount');
	$accountCollection->post('/subscriber/accounts/ip/list', 'staticiplist');
	
	/***** ACS apis *******/
	
	$accountCollection->post('/subscriber/acs/setup', 'createacsaccount');
	$accountCollection->post('/subscriber/acs/update', 'updateacsaccount');
	$accountCollection->post('/subscriber/acs/delete', 'deleteacsaccount');
	$accountCollection->post('/subscriber/acs/status', 'acsstatus');
        $accountCollection->post('/subscriber/acs/bulkstatus', 'acsbulkstatus');
	$accountCollection->post('/subscriber/acs/template', 'acstemplate');
	$accountCollection->post('/subscriber/acs/reboot', 'acsreboot');
	$accountCollection->post('/subscriber/acs/factoryreset', 'acsfactoryreset');
	$accountCollection->post('/subscriber/acs/event/queue', 'acseventqueue');
	$accountCollection->post('/subscriber/acs/event/log', 'acseventlog');
	$accountCollection->post('/subscriber/acs/config/push', 'acsconfigpush');
	$accountCollection->post('/subscriber/acs/devices/list', 'acsdevicelist');
        $accountCollection->post('/subscriber/acs/parameter/get', 'acsparameterget');
	/*************************/	
	return $accountCollection;

});
