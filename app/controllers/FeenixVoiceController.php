<?php
namespace Feenix\Controllers;
use Feenix\Exceptions\HTTPException;

class FeenixVoiceController extends RESTController{

	
	public function verify(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		return $voiceMyaccountObj->verifyMyKey();
	}
	
	public function login(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->loginMyAccount();
		return $this->respond($results);
	}

        public function sipupdate(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->updateSIPAccount();
                return $this->respond($results);
        }

        public function phoneupdate(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->updatePhoneAccount();
                return $this->respond($results);
        }

        public function phoneadd(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->addPhoneAccount();
                return $this->respond($results);
        }

        public function phoneavailable(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->availablePhoneNumber();
                return $this->respond($results);
        }


	public function createaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->createMyAccount();
		return $this->respond($results);
	}	

        public function callrate(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->MyCallRate();
                return $this->respond($results);
        }

        public function voicetimezone(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->MyAccountTimezone();
                return $this->respond($results);
        }


        public function calldetails(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->MyAccountCDR();
                return $this->respond($results);
        }


        public function useradclient(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->radclientSubscriber();
                return $this->respond($results);
        }

	public function createradiusaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->createSubscriber();
		return $this->respond($results);
	}	

	public function listradiusaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->listSubscriber();
		return $this->respond($results);
	}

	public function updateradiusaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->updateSubscriber();
		return $this->respond($results);
	}	


        public function renameradiusaccount(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->renameSubscriber();
                return $this->respond($results);
        }


	public function usageradiusaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->usageSubscriber();
		return $this->respond($results);
	}	

	public function radiusprofile(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->radiusProfile();
		return $this->respond($results);
	}	

	public function createradiusprofile(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->createProfile();
		return $this->respond($results);
	}	
	public function updateradiusprofile(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->updateProfile();
		return $this->respond($results);
	}	
	public function deleteradiusprofile(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->deleteProfile();
		return $this->respond($results);
	}	

	public function deleteradiusaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->deleteSubscriber();
		return $this->respond($results);
	}	

	public function staticiplist(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->listStaticIPs();
		return $this->respond($results);
	}	


	public function detailradiusaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->detailSubscriber();
		return $this->respond($results);
	}	

	public function createacsaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsCreateAccount();
		return $this->respond($results);
	}	

	public function acsstatus(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsStatus();
		return $this->respond($results);
	}
        public function acsbulkstatus() {
                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->acsBulkStatus();
                return $this->respond($results);
	}	

	public function deleteacsaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsDeleteAccount();
		return $this->respond($results);
	}	

	public function acstemplate(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsDeviceTemplates();
		return $this->respond($results);
	}	
	
	public function acsreboot(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsDeviceReboot();
		return $this->respond($results);
	}		

	public function acsfactoryreset(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsDeviceFactoryReset();
		return $this->respond($results);
	}	

	public function acseventqueue(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsEventQueue();
		return $this->respond($results);
	}	

	public function acseventlog(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsEventLog();
		return $this->respond($results);
	}		

	public function acsdevicelist(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsDeviceList();
		return $this->respond($results);
	}	

	public function updateacsaccount(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsDeviceUpdate();
		return $this->respond($results);
	}	

	public function acsconfigpush(){
		
		$voiceMyaccountObj = new \Feenix\Classes\Myaccount();
		$results  = $voiceMyaccountObj->acsDeviceConfigPush();
		return $this->respond($results);
	}

        public function acsparameterget(){

                $voiceMyaccountObj = new \Feenix\Classes\Myaccount();
                $results  = $voiceMyaccountObj->acsDeviceGetParameter();
                return $this->respond($results);
        }

		
	public function respond($results){
		return $results;
	}
}
