<?php

/**
 * Interface for Authentication  
 *
 * @category Feenix\Interfaces
 * @package Feenix\Interfaces
 */

namespace Feenix\Interfaces;

interface IAuth {

	/**
	 * Main method to Verify username and password
	 */
	public function verify($user,$pass);
	
}
