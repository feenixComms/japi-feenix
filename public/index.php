<?php

/**
 * FeeniX Voice APIs using PhalconPHP's Micro framework
 * 
 * @package None
 * @author  JEHANZAIB 
 * @contact	jehan@feenix.co.nz 
 * @license none
*/

date_default_timezone_set('Pacific/Auckland');

use Phalcon\DI\FactoryDefault as DefaultDI,
	Phalcon\Mvc\Micro\Collection,
	Phalcon\Config\Adapter\Ini as IniConfig,
	Phalcon\Loader;

use Phalcon\Logger\Adapter\File as FileAdapter;

$logger = new FileAdapter("../app/logs/api.log");
	
// Setup configuration files	
define('APP_DIR', dirname(__DIR__) . '/app');


/**
 * By default, namespaces are assumed to be the same as the path.
 * This function allows us to assign namespaces to alternative folders.
*/


$loader = new Loader();
$loader->registerNamespaces(array(
	'Feenix\Models' => APP_DIR . '/models/',
	'Feenix\Controllers' => APP_DIR . '/controllers/',
	'Feenix\Exceptions' => APP_DIR . '/exceptions/',
	'Feenix\Responses' => APP_DIR . '/responses/',
	'Feenix\Classes' => APP_DIR . '/classes/',
	'Feenix\Interfaces' => APP_DIR . '/interfaces/'
	
))->register();

/**
 * The DI is our direct injector.  It will store pointers to all of our services
 * and we will insert it into all of our controllers.
 * @var DefaultDI
*/
 
$di = new DefaultDI();

/**
 * Return array of the Collections, which define a group of routes, from
 * routes/collections.  These will be mounted into the app itself later.
 */

$di->set('collections', function(){
	return include(APP_DIR . '/routes/routeLoader.php');
});

/**
 * $di's setShared method provides a singleton instance.
 * If the second parameter is a function, then the service is lazy-loaded
 * on its first instantiation.
*/
 
$di->setShared('config', function() {
	return new IniConfig(APP_DIR . "/config/config.ini");
});


/**
 * Database setup.  Here, we'll use a simple SQLite database of Disney Princesses.
 */

/* 
 $di->set('db', function(){
	return new \Phalcon\Db\Adapter\Pdo\Sqlite(array(
		APP_DIR . '/data/database.sqlite'
	));
});
*/


/**
 * If our request contains a body, it has to be valid JSON.  This parses the 
 * body into a standard Object and makes that available from the DI.  If this service
 * is called from a function, and the request body is not valid JSON or is empty,
 * the program will throw an Exception.
*/

 

 $di->setShared('requestBody', function() {
	$in = file_get_contents('php://input');
	$in = json_decode($in, FALSE);

	// JSON body could not be parsed, throw exception
	if($in === null){
		throw new HTTPException(
			'There was a problem understanding the data sent to the server by the application.',
			409,
			array(
				'dev' => 'The JSON body sent to the server was unable to be parsed.',
				'internalCode' => '409',
				'more' => 'JSON body could not be parsed'
			)
		);
	}

	return $in;
});

/**
 * Our application is a Micro application, so we must explicitly define all the routes.
 * For APIs, this is ideal.  This is as opposed to the more robust MVC Application
 * @var $app
*/
 
$app = new Phalcon\Mvc\Micro();
$app->setDI($di);

/**
 * Before every request, make sure user is authenticated.
 * Returning true in this function resumes normal routing.
 * Returning false stops any route from executing.
*/

$app->before(function() use ($app, $di, $logger) {


	$req_comp = explode("/", $app->request->get('_url'));
	$req_app = ( isset($req_comp[1]) )? $req_comp[1] : "";

	//$logger->log( date("Y/m/d H:i:s") );
	$logger->log( print_r($app->request->get(), true) );


	if($app->request->get('id')){
	
		switch($req_app){
					
			case "voice":
													
							$voiceObj = new \Feenix\Controllers\FeenixVoiceController();
							
							if($voiceObj->verify()){
								return TRUE;
							}
							
					break;

			case "feenix":
													
							$voiceObj = new \Feenix\Controllers\FeenixVoiceController();
							
							if($voiceObj->verify()){
								return TRUE;
							}
							
					break;
			
			default:
						return FALSE;
					break;

		}

		throw new \Feenix\Exceptions\HTTPException(
														'Must login or provide credentials.',
														401,
														array(
																'dev' => 'Please provide API ID and Key in POST parameters.',
																'internalCode' => '401'
																//'more' => $app->getRouter()->getRewriteUri()
														)
		);
		
		return FALSE;
	}


	// All options requests get a 200, then die
	if($app->__get('request')->getMethod() == 'OPTIONS'){
		$app->response->setStatusCode(200, 'OK')->sendHeaders();
		exit;
	}

	// Exempted routes, such as login, or public info.  Let the route handler
	// pick it up.
	
	switch($app->getRouter()->getRewriteUri()){
		case '/':
			return true;
			break;
		
		/*	
		case '/feenix/v1/account/create':
			return true;
			break;
		*/
			
	}
	
	// If we made it this far, we have no valid auth method, throw a 401.
	throw new \Feenix\Exceptions\HTTPException(
		'Must login or provide credentials.',
		401,
		array(
			'dev' => 'Please provide credentials by either passing in a session token via cookie, or providing password and username via BASIC authentication.',
			'internalCode' => '401'
			//'more' => $app->getRouter()->getRewriteUri()
		)
		);

	return false;
});


/**
 * Mount all of the collections, which makes the routes active.
 */
foreach($di->get('collections') as $collection){
	$app->mount($collection);
}


/**
 * The base route return the list of defined routes for the application.
 * This is not strictly REST compliant, but it helps to base API documentation off of.
 * By calling this, you can quickly see a list of all routes and their methods.
 */
 
$app->post('/', function() use ($app){
	$routes = $app->getRouter()->getRoutes();
	$routeDefinitions = array('POST'=>array());
	foreach($routes as $route){
		$method = $route->getHttpMethods();
		$routeDefinitions[$method][] = $route->getPattern();
	}
	return $routeDefinitions;
});

$app->get('/', function() use ($app){
	$routes = $app->getRouter()->getRoutes();
	$routeDefinitions = array('GET'=>array());
	foreach($routes as $route){
		$method = $route->getHttpMethods();
		$routeDefinitions[$method][] = $route->getPattern();
	}
	return $routeDefinitions;
});

/**
 * After a route is run, usually when its Controller returns a final value,
 * the application runs the following function which actually sends the response to the client.
 *
 * The default behavior is to send the Controller's returned value to the client as JSON.
 * However, by parsing the request querystring's 'type' paramter, it is easy to install
 * different response type handlers.  Below is an alternate csv handler.
*/
 
$app->after(function() use ($app) {

	// OPTIONS have no body, send the headers, exit
	if($app->request->getMethod() == 'OPTIONS'){
		$app->response->setStatusCode('200', 'OK');
		$app->response->send();
		return;
	}

	// Respond by default as JSON

		// Results returned from the route's controller.  All Controllers should return an array
		$records = $app->getReturnedValue();

		$response = new \Feenix\Responses\JSONResponse();
		
		$response->useEnvelope(true) //this is default behavior
			->convertSnakeCase(false) //this is also default behavior
			->send($records);
		
		return;

});

/**
 * The notFound service is the default handler function that runs when no route was matched.
 * We set a 404 here unless there's a suppress error codes.
*/

$app->notFound(function () use ($app) {
	throw new \Feenix\Exceptions\HTTPException(
		'Not Found.',
		404,
		array(
			'dev' => 'That route was not found on the server.',
			'internalCode' => '401',
			'more' => 'Check route for mispellings.'
		)
	);
});

/**
 * If the application throws an HTTPException, send it on to the client as json.
 * Elsewise, just log it.
 * TODO:  Improve this.
 */
set_exception_handler(function($exception) use ($app){
	//HTTPException's send method provides the correct response headers and body
	if(is_a($exception, 'Feenix\\Exceptions\\HTTPException')){
		$exception->send();
	}
	error_log($exception);
	error_log($exception->getTraceAsString());
});

$app->handle();
